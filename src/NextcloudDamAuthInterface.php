<?php

namespace Drupal\nextcloud_dam;

/**
 * Interface to perform nextcloud authentification tasks.
 */
interface NextcloudDamAuthInterface {

  /**
   * Gets the Nextcloud credentials.
   *
   * @return array
   *   An array of credentialsfor the user
   *   with specified id or global if null
   *   array keys are server, user and key.
   */
  public function get();

}
