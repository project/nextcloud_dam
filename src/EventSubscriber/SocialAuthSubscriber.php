<?php

namespace Drupal\nextcloud_dam\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Event\LoginEvent;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth_nextcloud\NextcloudAuthManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Nextcloud DAM event subscriber.
 */
class SocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The social_auth.data_handler service.
   *
   * @var \Drupal\social_auth\SocialAuthDataHandler
   */
  protected $dataHandler;

  /**
   * The plugin.network.manager service.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $netManager;

  /**
   * The social_auth_nextcloud.manager service.
   *
   * @var \Drupal\social_auth_nextcloud\NextcloudAuthManager
   */
  protected $ncManager;

  /**
   * Constructs a SocialAuthSubscriber object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\social_auth\SocialAuthDataHandler $data_handler
   *   The social_auth.data_handler service.
   * @param \Drupal\social_api\Plugin\NetworkManager $net_manager
   *   The plugin.network.manager service.
   * @param \Drupal\social_auth_nextcloud\NextcloudAuthManager $nc_manager
   *   The social_auth_nextcloud.manager service.
   */
  public function __construct(MessengerInterface $messenger, SocialAuthDataHandler $data_handler, NetworkManager $net_manager, NextcloudAuthManager $nc_manager) {
    $this->messenger = $messenger;
    $this->dataHandler = $data_handler;
    $this->netManager = $net_manager;
    $this->ncManager = $nc_manager;
  }

  /**
   * Sets a drupal message when a user logs in.
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The Social Auth user event object.
   */
  public function onUserLogin(LoginEvent $event) {
    // dpm($event);
    $this->messenger->addStatus('User has logged in with a Social Auth implementer. Implementer for ' . $event->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SocialAuthEvents::USER_LOGIN => 'onUserLogin',
    ];
  }

}
