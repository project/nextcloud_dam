<?php

namespace Drupal\nextcloud_dam\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Nextcloud DAM routes.
 */
class UpdateMetadataController extends ControllerBase {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager')
      );
  }

  /**
   * Executes the updateMetadata for one item.
   *
   * To look for batch run look at NextcloudediaUsage Actions run.
   *
   * @param \Drupal\Core\Entity\EntityInterface $media
   *   The media to update source metadata.
   *
   * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirection response or null.
   */
  public function execute(EntityInterface $media) {
    // Reusing the action used in queueworker.
    $action = $this->entityTypeManager->getStorage('action')->load('nextcloud_metadata');

    $batch = [
      'title' => $this->t('Updating Metadata'),
      'operations' => [
      [[__CLASS__, 'executeUpdateMetadataAction'], [$action, $media]],
      ],
      'finished' => [__CLASS__, 'finishedUpdateMetadataAction'],
    ];
    batch_set($batch);
    // @todo read destination from url
    return batch_process();
  }

  /**
   * Callback to be called by batch_set operation.
   */
  public static function executeUpdateMetadataAction($action, $media, &$context) {
    if (!$action->getPlugin()->access($media)) {
      \Drupal::messenger()->addError(
            t(
                'No access to execute %action on the @entity_type_label %entity_label.', [
                  '%action' => $action->label(),
                  '@entity_type_label' => $media->getEntityType()->getLabel(),
                  '%entity_label' => $media->label(),
                ]
            )
        );
      $context['finished'] = 1;
    }
    $action->execute([$media]);
    $context['finished'] = 1;
  }

  /**
   * Callback to be called by batch_set finished.
   */
  public static function finishedUpdateMetadataAction($success, $results, $operations) {
    \Drupal::messenger()
      ->addStatus(t('Media metadata have been ran.'));
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Entity\EntityInterface $media
   *   The media to update source metadata.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, EntityInterface $media) {
    // Check permissions and combine that with any custom access checking
    // needed. Pass forward parameters from the route and/or request as needed.
    $action = $this->entityTypeManager->getStorage('action')->load('nextcloud_metadata');

    return AccessResult::allowedIf($account->hasPermission('refresh media nextcloud metadata') && $action->getPlugin()->access($media));
  }

}
