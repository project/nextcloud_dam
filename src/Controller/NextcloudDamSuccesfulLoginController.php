<?php

namespace Drupal\nextcloud_dam\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Nextcloud DAM routes.
 */
class NextcloudDamSuccesfulLoginController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('You could close this window now'),
    ];

    return $build;
  }

}
