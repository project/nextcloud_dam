<?php

namespace Drupal\nextcloud_dam\Exception;

use Drupal\Core\Url;

/**
 * Exception indicating that the selected bundle does not exist.
 */
class BundleNotExistException extends NextcloudDamException {

  /**
   * {@inheritdoc}
   */
  protected $adminPermission = 'administer entity browsers';

  /**
   * Constructs BundleNotExistException.
   *
   * @param string $bundle
   *   Name of the bundle.
   */
  public function __construct($bundle) {
    $log_message_args = [
      ':eb_conf' => Url::fromRoute('entity.entity_browser.collection')
        ->toString(),
      '@bundle' => $bundle,
    ];
    $admin_message = 'Media type @bundle does not exist. Please fix the Nextcloud DAM <a href=":eb_conf">widget configuration</a>.';
    $message = sprintf('Nextcloud DAM integration incorrect. Media type %s does not exist.', $bundle);
    $user_message = 'Nextcloud DAM integration incorrect. Media type @bundle does not exist.';
    parent::__construct(
          $message,
          $admin_message,
          $user_message,
          $log_message_args
      );
  }

}
