<?php

namespace Drupal\nextcloud_dam\Exception;

/**
 * Exception indicating that there are errors with server definition.
 */
class NextcloudServerDefinitionException extends \Exception {
}
