<?php

namespace Drupal\nextcloud_dam\Exception;

/**
 * Exception indicating that there are errors when trying to connect.
 */
class NextcloudConnectException extends \Exception {
}
