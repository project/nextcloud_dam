<?php

namespace Drupal\nextcloud_dam\Exception;

use Drupal\Core\Url;

/**
 * Exception indicating that the bundle does not represent NextcloudDam assets.
 */
class BundleNotNextcloudException extends NextcloudDamException {

  /**
   * {@inheritdoc}
   */
  protected $adminPermission = 'administer entity browsers';

  /**
   * Constructs BundleNotNextcloudException.
   *
   * @param string $bundle
   *   Name of the bundle.
   */
  public function __construct($bundle) {
    $log_message_args = [
      ':eb_conf' => Url::fromRoute('entity.entity_browser.collection')
        ->toString(),
      '@bundle' => $bundle,
    ];
    $admin_message = 'Media type @bundle is not using Nextcloud plugin. Please fix the Nextcloud DAM <a href=":eb_conf">widget configuration</a>.';
    $message = sprintf('Media type %s is not using Nextcloud plugin. Please fix the Nextcloud DAM entity browser widget.', $bundle);
    $user_message = 'Nextcloud DAM integration is not configured correctly. Please contact the site administrator.';
    parent::__construct(
          $message,
          $admin_message,
          $user_message,
          $log_message_args
      );
  }

}
