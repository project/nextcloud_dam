<?php

namespace Drupal\nextcloud_dam\Exception;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base exception class for Nextcloud DAM.
 */
abstract class NextcloudDamException extends \Exception {

  use StringTranslationTrait;


  /**
   * Admin permission related to this exception.
   *
   * @var string
   */
  protected $adminPermission = 'administer nextcloud_dam configuration';

  /**
   * Message level to be used when displaying the message to the user.
   *
   * @var string
   */
  protected $messageLevel = 'error';

  /**
   * User-facing for admin users.
   *
   * @var string
   */
  protected $adminMessage;

  /**
   * User-facing for non-admin users.
   *
   * @var string
   */
  protected $userMessage;

  /**
   * Arguments for the log message.
   *
   * @var array
   */
  protected $logMessageArgs;

  /**
   * Constructs BundleNotExistException.
   */
  public function __construct(
    $message,
    $admin_message = NULL,
    $user_message = NULL,
    $log_message_args = [],
  ) {
    $this->adminMessage = $admin_message ?: $message;
    $this->userMessage = $user_message ?: $message;
    $this->logMessageArgs = $log_message_args;
    parent::__construct($message);
  }

  /**
   * Displays message to the user.
   */
  public function displayMessage() {
    // It's not clear how can a service must be injected in an exception, also
    // it would be great to use.
    // @phpstan-ignore globalDrupalDependencyInjection.useDependencyInjection
    // But is not working...
    // @phpstan-ignore-next-line
    if (\Drupal::currentUser()->hasPermission($this->adminPermission)) {
      // @phpstan-ignore-next-line
      \Drupal::messenger()->addError($this->adminMessage);
    }
    else {
      // @phpstan-ignore-next-line
      \Drupal::messenger()->addError($this->userMessage);
    }
  }

  /**
   * Logs exception into Drupal's log.
   *
   * @return \Drupal\nextcloud_dam\Exception\NextcloudDamException
   *   This exception.
   */
  public function logException() {
    // @phpstan-ignore-next-line
    \Drupal::logger('nextcloud_dam')->error($this->adminMessage, $this->logMessageArgs);
    return $this;
  }

}
