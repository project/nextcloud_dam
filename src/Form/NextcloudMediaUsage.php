<?php

namespace Drupal\nextcloud_dam\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\nextcloud_dam\NextcloudDamApiInterface;
use Drupal\nextcloud_dam\Plugin\media\Source\Nextcloud;
use Drupal\node\NodeInterface;
use Drupal\system\ActionConfigEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller contains methods for displaying nextcloud media usage info.
 */
class NextcloudMediaUsage extends FormBase {

  /**
   * The bulk operations.
   *
   * @var \Drupal\system\ActionConfigEntityInterface[]
   *
   * @todo Change the typehint to ActionConfigEntityInterface when
   *   https://www.drupal.org/project/drupal/issues/3017214 is in.
   */
  protected $actions;

  /**
   * The nextcloud_dam API service.
   *
   * @var \Drupal\nextcloud_dam\NextcloudDamApiInterface
   */
  protected $nextcloudDam;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Renderer object.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entity type manager under test.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a NextcloudMediaUsage class instance.
   *
   * @param \Drupal\nextcloud_dam\NextcloudDamApiInterface $nextcloud_dam
   *   The nextcloud_dam API service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer_object
   *   Renderer object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(NextcloudDamApiInterface $nextcloud_dam, ConfigFactoryInterface $config_factory, RendererInterface $renderer_object, EntityTypeManagerInterface $entity_type_manager) {
    $this->nextcloudDam = $nextcloud_dam;
    $this->configFactory = $config_factory;
    $this->renderer = $renderer_object;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('nextcloud_dam.api'),
          $container->get('config.factory'),
          $container->get('renderer'),
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nextcloud_media_usage_in_entity';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NodeInterface $node = NULL) {

    // Filter the actions to only include those for nextcloud_metadata.
    $this->actions = array_filter(
          $this->entityTypeManager->getStorage('action')->loadMultiple(), function (ActionConfigEntityInterface $action) {
              return $action->id() == "nextcloud_metadata";
          }
      );

    $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
    $nextcloud_types = array_filter(
          $media_types, function ($type) {
              /**
              * @var \Drupal\media\MediaTypeInterface $type
              */
              return $type->getSource() instanceof Nextcloud;
          }
      );
    $entity_reference_fields = array_filter(
          $node->getFields(), function ($field) {
              return $field instanceof EntityReferenceFieldItemList;
          }
      );
    $header = [
      'name' => ['data' => $this->t('Name'), 'specifier' => 'name'],
      'type' => ['data' => $this->t('Remote Type'), 'specifier' => 'type'],
      'media_type' => [
        'data' => $this->t('Media type'),
        'specifier' => 'media_type',
      ],
      'operations' => $this->t('Operations'),
    ];
    $rows = [];
    $entities = [];

    /**
     * @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field
     */
    foreach ($entity_reference_fields as $field) {
      /**
       * @var \Drupal\media\Entity\Media $entity
       */
      foreach ($field->referencedEntities() as $entity) {
        if (in_array($entity->bundle(), array_keys($nextcloud_types))) {
          $entities[] = $entity;
          $name = $entity->getSource()->getMetadata($entity, 'label');
          $type = $entity->getSource()->getMetadata($entity, 'item_type');
          $nextcloud_id = $entity->getSource()->getMetadata($entity, 'id');
          $nextcloud_item_source = $entity->getSource()->getMetadata($entity, 'item_source');
          $nextcloud_server = $entity->getSource()->getMetadata($entity, 'server');
          $nextcloud_side_file_uri = ($nextcloud_server && $nextcloud_item_source) ? "$nextcloud_server/f/$nextcloud_item_source" : NULL;
          $nextcloud_edit_url = ($nextcloud_side_file_uri) ? Url::fromUri($nextcloud_side_file_uri) : '';
          $row['name'] = $name ?: $nextcloud_id ?: $this->t('N/A');
          $row['type'] = $type ?: $this->t('N/A');
          $row['media_type'] = $entity->bundle() ?: $this->t('N/A');
          $links['edit'] = [
            'title' => $this->t('Edit'),
            'url' => $entity->toUrl('edit-form'),
          ];
          $links['edit_on_nextcloud_dam'] = [
            'title' => $this->t('edit on nextcloud'),
            'url' => $nextcloud_edit_url,
          ];
          $links['update_metadata'] = [
            'title' => $this->t('update metadata'),
            'url' => Url::fromRoute('nextcloud_dam.media.update_nextcloud_metadata', ['media' => $entity->id()], ['query' => ['destination' => "/node/{$node->id()}/nextcloud-usage"]]),
          ];
          $row['operations']['data'] = [
            '#type' => 'operations',
            '#links' => $links,
          ];
          $rows[$entity->id()] = $row;
        }
      }
    }

    $form['media_using_nextcloud'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t("There are no nextcloud media found on the page."),
    ];
    $this->renderer->addCacheableDependency($form, $node);
    // Whenever nextcloud_dam.settings config form is there:
    // $this->renderer->addCacheableDependency($form,
    // $this->configFactory->get('nextcloud_dam.settings'));.
    foreach ($entities as $entity) {
      $this->renderer->addCacheableDependency($form, $entity);
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply to selected items'),
      '#button_type' => 'primary',
    ];

    // Ensure a consistent container for filters/operations in the view header.
    $form['header'] = [
      '#type' => 'container',
      '#weight' => -100,
    ];

    $action_options = [];
    foreach ($this->actions as $id => $action) {
      $action_options[$id] = $action->label();
    }
    $form['header']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Action'),
      '#options' => $action_options,
    ];
    // Duplicate the form actions into the action container in the header.
    $form['header']['actions'] = $form['actions'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $selected = array_filter($form_state->getValue("media_using_nextcloud"));
    if (empty($selected)) {
      $form_state->setErrorByName('', $this->t('No items selected.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected = array_filter($form_state->getValue("media_using_nextcloud"));
    $entities = [];
    $action = $this->actions[$form_state->getValue('action')];
    $count = 0;
    $entities = $this->entityTypeManager->getStorage('media')->loadMultiple($selected);
    foreach ($selected as $id) {
      $entity = $entities[$id];
      // Skip execution if the user did not have access.
      if (!$action->getPlugin()->access($entity)) {
        $this->messenger()->addError(
              $this->t(
                  'No access to execute %action on the @entity_type_label %entity_label.', [
                    '%action' => $action->label(),
                    '@entity_type_label' => $entity->getEntityType()->getLabel(),
                    '%entity_label' => $entity->label(),
                  ]
              )
          );
        continue;
      }

      $count++;
      $entities[$id] = $entity;
    }

    // Don't perform any action unless there are some elements affected.
    // @see https://www.drupal.org/project/drupal/issues/3018148
    if (!$count) {
      return;
    }

    $action->execute($entities);

    $operation_definition = $action->getPluginDefinition();
    if (!empty($operation_definition['confirm_form_route_name'])) {
      $options = [
        'query' => $this->getDestinationArray(),
      ];
      $form_state->setRedirect($operation_definition['confirm_form_route_name'], [], $options);
    }
    else {
      $this->messenger()->addStatus(
            $this->formatPlural(
                $count, '%action was applied to @count item.', '%action was applied to @count items.', [
                  '%action' => $action->label(),
                ]
            )
        );
    }
  }

}
