<?php

namespace Drupal\nextcloud_dam\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class FieldStorageDeleteConfirmForm extends ConfirmFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a ModulesListConfirmForm object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $logger_factory) {
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_factory;
    $this->logger = $this->loggerFactory->get('nextcloud_dam');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('module_handler'), $container->get('logger.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nextcloud_dam_field_storage_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t("Are you sure you want to delete the nextcloud_metadata field storage?");
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_config');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this
      ->t("Nextcloud DAM module will be completely unusable and you'll need to reinstall it again to recreate it. This action cannot be undone.");
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->moduleHandler->loadInclude('nextcloud_dam', 'install');
      nextcloud_dam_uninstall();
      $this->messenger()->addStatus($this->t('The field storage definition for nextcloud assets metadata is now deleted. You should uninstall Nextcloud DAM module now as it will not properly work as it should.'));
      $form_state->setRedirectUrl(new Url('system.modules_uninstall'));
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
      $this->messenger()->addError($this->t('Something went wrong while deleting field storage definition for nextcloud assets metadata.') . $e->getMessage());
    }
  }

}
