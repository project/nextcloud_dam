<?php

namespace Drupal\nextcloud_dam;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\SocialAuthDataHandler;

/**
 * Nextcloud Authentication service.
 */
class NextcloudDamAuth implements NextcloudDamAuthInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Drupal\social_auth\SocialAuthDataHandler.
   *
   * @var Drupal\social_auth\SocialAuthDataHandler
   */
  protected $socialAuthDataHandler;

  /**
   * The network manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  private NetworkManager $networkManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Constructs a new WebdavsOauth object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, SocialAuthDataHandler $social_auth_data_handler, EntityFieldManager $entity_field_manager, NetworkManager $network_manager) {
    $this->config = $config_factory;
    $this->socialAuthDataHandler = $social_auth_data_handler;
    $this->entityFieldManager = $entity_field_manager;
    $this->networkManager = $network_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    $networks = [];
    foreach ($this->networkManager->getDefinitions() as $definition) {
      // If ($session url == network-url)
      $networks[$definition['id']]['network'] = $this->networkManager->createInstance($definition['id']);
      $this->socialAuthDataHandler->setSessionPrefix($definition['id']);
      if ($access_token = $this->socialAuthDataHandler->get('access_token')) {
        /*if (
        $accesstoken->getExpires() < (new DrupalDateTime())->getTimestamp() ) {
        $newAccessToken = $credentials['access_token']
        ->getAccessToken('refresh_token', [
        'refresh_token' => $credentials['access_token']->getRefreshToken()
        ]);*/

        $networks[$definition['id']]['access_token'] = $access_token;
      }
    }

    return $networks;
  }

}
