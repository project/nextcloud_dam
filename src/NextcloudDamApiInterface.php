<?php

namespace Drupal\nextcloud_dam;

/**
 * Interface for Nextcloud Api.
 */
interface NextcloudDamApiInterface {

  /**
   * Obtain the client to perform api calls.
   *
   * Server string
   *   the server to operate.
   * $uid integer
   *   the user id that will be used for calling api.
   * $maintainer bool
   *   If needs to perform maintainer tasks so the prepared user will be used.
   *
   * @return Drupal\nextcloud_dam\NextcloudDamApi
   *   the same object as this will be used to chain.
   *
   * @throws Drupal\nextcloud_dam\NextcloudConnectException
   */
  public function setClient($server, $uid, bool $maintainer);

  /**
   * Gets the server used for this api connection.
   *
   * @return string
   *   The address of the used server
   */
  public function getServer();

  /**
   * Checks api connection.
   *
   * @return bool
   *   If connection could be done
   */
  public function check();

  /**
   * Gets the file information and shares from share id.
   *
   * It's based on contributed nextcloud app that allows getting it via api
   * call.
   *
   * @param int $share_id
   *   The share id to lookup.
   *
   * @return array
   *   With the information of the shared item as  share api provides.
   */
  public function getShareById($share_id);

  /**
   * Gets the shares of a resource( file or  dir).
   *
   * @param string $path
   *   The path to check for.
   * @param bool $reshares
   *   = TRUE
   *   If must include reshares shares in the query.
   * @param bool $subfiles
   *   = TRUE
   *   If the query must be recursive when directories.
   *
   * @return string
   *   The json result of the query, same as in
   *   https://docs.nextcloud.com/server/18/developer_manual/client_apis/OCS/ocs-share-api.html#get-shares-from-a-specific-file-or-folder
   *
   * @throws \Drupal\nextcloud_dam\Exception\NextcloudConnectException
   *   If unable to connect.
   */
  public function shares($path, bool $reshares, bool $subfiles);

  /**
   * Share a resource with the specified options.
   *
   * @param array $share_options
   *   The share options as in
   *   https://docs.nextcloud.com/server/18/developer_manual/client_apis/OCS/ocs-share-api.html#create-a-new-share.
   *
   * @returns string with the status code and message:
   *    100 - successful
   *    400 - unknown share type
   *    403 - public upload was disabled by the admin
   *    404 - file couldn’t be shared
   *
   * @throws \Drupal\nextcloud_dam\Exception\NextcloudConnectException
   *   If unable to connect.
   */
  public function share($share_options);

  /**
   * Gets the file information and shares from an file id.
   *
   * It's based on contributed nextcloud app that allows getting it via api
   * call.
   *
   * @param int $file_id
   *   The file id to lookup.
   *
   * @return array
   *   With the information of the file and its shares as fileinfo provides and
   *   share api provides.
   */
  public function shareByFileId($file_id);

  /**
   * Delete a shared resource.
   *
   * @param int $share_id
   *   The share id to delete.
   *
   * @returns string with the status code and message:
   *    100 - successful
   *    404 - file couldn’t be deleted
   *
   * @throws \Drupal\nextcloud_dam\Exception\NextcloudConnectException
   *   If unable to connect.
   */
  public function deleteShare($share_id);

  /**
   * Share a editor file from path.
   *
   * @param array $path
   *   The path to file.
   *
   * @returns string with the status code and message:
   *    100 - successful
   *    400 - unknown share type
   *    403 - public upload was disabled by the admin
   *    404 - file couldn’t be shared
   *
   * @throws \Drupal\nextcloud_dam\Exception\NextcloudConnectException
   *   If unable to connect.
   *
   * @see NC/server/apps/files/lib/Controller/DirectEditingController.php*
   */
  public function openDirectEditUrl($path);

}
