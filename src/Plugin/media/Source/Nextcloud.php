<?php

namespace Drupal\nextcloud_dam\Plugin\media\Source;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Error;
use Drupal\file\FileRepositoryInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\nextcloud_dam\NextcloudDamApiInterface;
use Drupal\nextcloud_dam\Plugin\Field\FieldType\NextcloudMetadataItem;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media source plugin for Nextcloud.
 *
 * @MediaSource(
 *   id = "nextcloud",
 *   label = @Translation("Nextcloud"),
 *   description = @Translation("Provides business logic and metadata for Nextcloud."),
 *   default_thumbnail_filename = "nextcloud_dam_no_image.png",
 *   thumbnail_alt_metadata_attribute = "file_target",
 *   allowed_field_types = {"string", "string_long"},
 *   forms = {
 *     "media_library_add" = "Drupal\nextcloud_dam\Form\NextcloudDamMediaForm"
 *   }
 * )
 */
class Nextcloud extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * NextcloudDam api service.
   *
   * @var \Drupal\nextcloud_dam\nextcloudDamInterface
   *   NextcloudDam api service.
   */
  protected $nextcloudDam;

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Statically cached API response for a given asset.
   *
   * @var array
   */
  protected $apiResponse;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Statically cached metadata information for the given assets.
   *
   * @var array
   */
  protected $metadata;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The HTTP client to fetch the files with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\nextcloud_dam\nextcloudDamInterface $nextcloud_dam_api_service
   *   NextcloudDam api service.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManagerInterface $field_type_manager, ConfigFactoryInterface $config_factory, NextcloudDamApiInterface $nextcloud_dam_api_service, AccountProxyInterface $account_proxy, UrlGeneratorInterface $url_generator, LoggerChannelFactoryInterface $logger, CacheBackendInterface $cache, ModuleHandlerInterface $module_handler, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);

    $this->nextcloudDam = $nextcloud_dam_api_service;
    $this->accountProxy = $account_proxy;
    $this->urlGenerator = $url_generator;
    $this->logger = $logger;
    $this->cache = $cache;
    $this->moduleHandler = $module_handler;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('entity_type.manager'),
          $container->get('entity_field.manager'),
          $container->get('plugin.manager.field.field_type'),
          $container->get('config.factory'),
          $container->get('nextcloud_dam.api'),
          $container->get('current_user'),
          $container->get('url_generator'),
          $container->get('logger.factory'),
          $container->get('cache.data'),
          $container->get('module_handler'),
          $container->get('file_system'),
          $container->get('file.repository'),
          $container->get('http_client')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    // https://lukasreschke.github.io/OpenCloudMeshSpecification/#get-information-about-a-share
    // https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/ocs-share-api.html
    // https://pub.dev/documentation/nextcloud/latest/nextcloud/ShareTypes-class.html
    $fields = [
    // Unique identified of the share (integer),.
      "id" => $this->t('id'),
      "share_type" => $this->t('share type'),
      "uid_owner" => $this->t('uid owner'),
      "displayname_owner" => $this->t('displayname owner'),
      "permissions" => $this->t('permissions'),
      "can_edit" => $this->t('can edit'),
      "can_delete" => $this->t('can delete'),
      "stime" => $this->t('stime'),
      "parent" => $this->t('parent'),
      "expiration" => $this->t('expiration'),
      "token" => $this->t('token'),
      "uid_file_owner" => $this->t('uid file owner'),
      "note" => $this->t('note'),
      "label" => $this->t('label'),
      "displayname_file_owner" => $this->t('displayname file owner'),
      "path" => $this->t('Path'),
    // The item_type either "file" or "folder" (string),.
      "item_type" => $this->t('Item type'),
      "mimetype" => $this->t('mimetype'),
      "has_preview" => $this->t('has preview'),
      "storage_id" => $this->t('storage id'),
      "storage" => $this->t('storage'),
      "item_source" => $this->t('item source'),
      "file_source" => $this->t('file source'),
      "file_parent" => $this->t('file parent'),
      "file_target" => $this->t('file target'),
      "share_with" => $this->t('share with'),
      "share_with_displayname" => $this->t('share with displayname'),
      "password" => $this->t('password'),
      "send_password_by_talk" => $this->t('send password by talk'),
      "url" => $this->t('url'),
      "mail_send" => $this->t('mail send'),
      "hide_download" => $this->t('hide download'),
      "attributes" => $this->t('attributes'),
      "server" => $this->t('server'),
      "file_name" => $this->t('escaped file name'),
    ];

    return $fields;
  }

  /**
   * Returns a list of remote metadata properties.
   *
   * @return array
   *   list of remote metadata properties.
   */
  public function getRemoteMetadataProperties() {
    return [
      'id',
      'share_type',
      'uid_owner',
      'displayname_owner',
      'permissions',
      'can_edit',
      'can_delete',
      'stime',
      'parent',
      'expiration',
      'token',
      'uid_file_owner',
      'note',
      'label',
      'displayname_file_owner',
      'path',
      'item_type',
      'mimetype',
      'has_preview',
      'storage_id',
      'storage',
      'item_source',
      'file_source',
      'file_parent',
      'file_target',
      'share_with',
      'share_with_displayname',
      'password',
      'send_password_by_talk',
      'url',
      'mail_send',
      'hide_download',
      'attributes',
      // self-cooked.
      'server',
      'thumbnail_relationship',
      'file_name',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => '',
    ];
  }

  /**
   * Ensures the given media entity has Nextcloud metadata information in place.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   * @param bool $force
   *   (optional) By default, this will not attempt to check for updated
   *   metadata if there is local data available. Pass TRUE to always check for
   *   changed metadata.
   *
   * @return bool
   *   TRUE if the metadata is ensured. Otherwise, FALSE.
   */
  public function ensureMetadata(MediaInterface $media, $force = FALSE) {
    $media_url = $this->getSourceFieldValue($media);
    if (!empty($this->metadata[$media_url]) && !$force) {
      return TRUE;
    }
    // If we cannot get a value for $media_uuid we should better exit early.
    // This helps avoid circular references introduced due to ::ensureMetadata()
    // calling hook_nextcloud_media_update_alter(), which in turn may call this
    // method in a loop.
    if (empty($media_url)) {
      return FALSE;
    }

    if (!$media->hasField(NextcloudMetadataItem::METADATA_FIELD_NAME)) {
      $this->logger->get('nextcloud_dam')->error(
            'The media type @type must have a Nextcloud metadata field named "nexcloud_metadata".', [
              '@type' => $media->bundle(),
            ]
        );
      return FALSE;
    }

    if (!$media->get(NextcloudMetadataItem::METADATA_FIELD_NAME)->isEmpty() && !$force) {
      $metadata = Json::decode($media->get(NextcloudMetadataItem::METADATA_FIELD_NAME)->value);
      if (is_array($metadata)) {
        $this->metadata[$media_url] = $metadata;
        return TRUE;
      }
    }

    return $this->fillMetadata($media, $metadata, $metadata['relationship']);
  }

  /**
   * Fills the metadata of a media.
   *
   * @param Drupal\media\MediaInterface $media
   *   The media that will income all the metadata.
   * @param array $metadata
   *   The metadata that coming from a ocs share retrieval.
   * @param string $relationship
   *   The type of relationship between the media and the thumbnail (it should
   *   be link, copy(default), or integrated):
   *     - link, will set the thumbnail as link to remote media.
   *     - copy, will fetch the thumbnail and will allow set image styles after
   *       as it will live in drupal as a file.
   *     - integrated, will be added as an iframe (useful for some kind of
   *       documents. e.g.)
   *
   * @return bool
   *   If succesful updating.
   */
  public function fillMetadata($media, $metadata, $relationship = 'link') {
    $media_url = $this->getSourceFieldValue($media);
    $this->logger->get('nextcloud_dam')->error(
          'Refresh of the metadata of the share url @url from remote origin must be done by client.', [
            '@url' => $media_url,
          ]
      );

    // Inject server url.
    $metadata['server'] = isset($metadata['url']) ? explode("/s/", $metadata['url'])[0] : NULL;
    // Inject the relationship.
    $metadata['thumbnail_relationship'] = $relationship;
    // Inject the filename escaped from file_target.
    $metadata['file_name'] = (!empty($metadata['file_target'])) ? pathinfo($metadata['file_target'])['filename'] : NULL;

    $this->metadata[$media_url] = $this->filterRemoteMetadata($metadata);

    $has_changed = FALSE;
    if ($this->hasMetadataChanged($media, $this->metadata[$media_url])) {
      $encoded_metadata = Json::encode($this->metadata[$media_url]);
      if (!$encoded_metadata) {
        $this->logger->get('nextcloud_dam')->error(
              'Unable to JSON encode the returned API response for the media URL @url.', [
                '@url' => $media_url,
              ]
          );

        return FALSE;
      }

      $media->set(NextcloudMetadataItem::METADATA_FIELD_NAME, $encoded_metadata);
      $has_changed = TRUE;
    }

    // Allow other modules to alter the media entity.
    $this->moduleHandler->alter('nextcloud_media_update', $media, $has_changed);

    // If the media entity is still new it will be saved and we don't have
    // do that twice.
    if ($has_changed && !$media->isNew()) {
      $media->save();
    }

    return $has_changed;

  }

  /**
   * Returns a list of filtered remote metadata properties.
   *
   * @param array $metadata
   *   The metadata items.
   *
   * @return array
   *   Filtered list of remote metadata properties.
   */
  public function filterRemoteMetadata(array $metadata) {
    return array_intersect_key($metadata, array_combine($this->getRemoteMetadataProperties(), $this->getRemoteMetadataProperties()));
  }

  /**
   * Compares the local metadata and the remote metadata in case it changed.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   * @param array $remote_metadata
   *   The remote metadata.
   *
   * @return bool
   *   TRUE if the remote metadata has changed. Otherwise, FALSE.
   */
  public function hasMetadataChanged(MediaInterface $media, array $remote_metadata) {
    $remote_metadata = $this->filterRemoteMetadata($remote_metadata);
    if ($media->get(NextcloudMetadataItem::METADATA_FIELD_NAME)->isEmpty() && !empty($remote_metadata)) {
      return TRUE;
    }

    $local_metadata = (array) Json::decode((string) $media->get(NextcloudMetadataItem::METADATA_FIELD_NAME)->value);
    return $local_metadata !== $remote_metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name = NULL) {
    $remote_url = $this->getSourceFieldValue($media);

    // Ensure the metadata information are fetched.
    if ($this->ensureMetadata($media)) {
      switch ($name) {
        case 'video_preview_urls':
          return $this->metadata[$remote_url]['videoPreviewURLs'] ?? NULL;

        case 'thumbnail_urls':
          return $this->metadata[$remote_url]['thumbnails'] ?? NULL;

        case 'thumbnail_uri':
          if (!empty($this->metadata[$remote_url]['url']) && !empty($this->metadata[$remote_url]['server']) && !empty($this->metadata[$remote_url]['token'])) {
            $preview_thumbnail = $this->metadata[$remote_url]['url'] . "/preview?x=300";
            // $sized_preview = $this->metadata[$remote_url]['server'] .
            // "/apps/files_sharing/publicpreview/" .
            // $this->metadata[$remote_url]['token'] .
            // '?x=320&y=200&a=true';
            // Also:
            // https://nextcloud.example.org/s/aabbccddd/video_preview_urls ?
            //
            // @todo get configuration of entity_browser about link or copy
            // here.
            // ¿ querying $this->useRemoteImages may be a way?
            // Default to Link.
            if ($this->metadata[$remote_url]['thumbnail_relationship'] == 'link' or empty($this->metadata[$remote_url]['thumbnail_relationship'])) {
              return $preview_thumbnail;

              // May be useful to add settings when wanting to confine in
              // dedicated directory ??
            }
            else {
              $image_url = $preview_thumbnail;
              $destination = $this->configFactory->get('system.file')->get('default_scheme') . '://';
              if ($this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
                $files = $this->fileSystem->scanDirectory($destination, "/^{$this->metadata[$remote_url]['token']}/");
                // Scan for existing scanned thumbnail.
                if (count($files) > 0) {
                  return reset($files)->uri;
                }
              }
              try {
                $data = (string) $this->httpClient->get($image_url)->getBody();
                $file = $this->fileRepository->writeData($data, $destination, TRUE);
                return $file->getFileUri();
              }
              catch (\Exception $exception) {
                Error::logException($this->logger->get('nextcloud_dam'), $exception);
              }
            }
          }
          return parent::getMetadata($media, 'thumbnail_uri');

        case 'created':
          return $this->metadata[$remote_url]['dateCreated'] ?? NULL;

        case 'modified':
          return $this->metadata[$remote_url]['dateModified'] ?? NULL;

        case 'default_name':
          return $this->metadata[$remote_url]['name'] ?? parent::getMetadata($media, 'default_name');

        case 'mimetype':
          return isset($this->metadata[$remote_url]['mimetype']) ? substr($this->metadata[$remote_url]['mimetype'], 0, 5) : 'document';

        default:
          return $this->metadata[$remote_url][$name] ?? NULL;
      }
    }

    return parent::getMetadata($media, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Check the connection with nextcloud_dam.
    try {
      // @todo , perform check before configuring
      $this->nextcloudDam->check();
    }
    catch (\Exception $exception) {
      if ($this->accountProxy->hasPermission('administer nextcloud_dam configuration')) {
        /* drupal_set_message(
        $this->t(
        'Connecting with NextcloudDam failed. Check if the configuration is
        set properly <a href=":url">here</a>.', [
        ':url' => $this->urlGenerator->generateFromRoute('nextcloud_dam.admin'),
        ]
        ), 'error'
        );
         */
      }
      else {
        $this->messenger()->addError($this->t('Something went wrong with the NextcloudDam connection. Please contact the site administrator.'));
      }
    }

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Creates the metadata field storage definition.
   *
   * @return \Drupal\field\FieldStorageConfigInterface
   *   The unsaved field storage definition.
   */
  public function createMetadataFieldStorage() {
    return $this->entityTypeManager->getStorage('field_storage_config')
      ->create(
              [
                'entity_type' => 'media',
                'field_name' => NextcloudMetadataItem::METADATA_FIELD_NAME,
                'type' => 'nextcloud_metadata',
                'cardinality' => 1,
                'locked' => TRUE,
              ]
          );
  }

  /**
   * Creates the metadata field definition.
   *
   * @param \Drupal\media\MediaTypeInterface $type
   *   The media type.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   The unsaved field definition. The field storage definition, if new,
   *   should also be unsaved.
   */
  public function createMetadataField(MediaTypeInterface $type) {
    return $this->entityTypeManager->getStorage('field_config')
      ->create(
              [
                'entity_type' => 'media',
                'field_name' => NextcloudMetadataItem::METADATA_FIELD_NAME,
                'bundle' => $type->id(),
                'label' => 'Nextcloud Metadata',
                'translatable' => FALSE,
                'field_type' => 'nextcloud_metadata',
              ]
          );
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'nextcloud_media' => [],
    ];
  }

}
