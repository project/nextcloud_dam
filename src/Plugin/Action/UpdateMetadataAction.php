<?php

namespace Drupal\nextcloud_dam\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\Plugin\Action\EntityActionBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Error;
use Drupal\nextcloud_dam\Exception\NextcloudServerDefinitionException;
use Drupal\nextcloud_dam\Plugin\Field\FieldType\NextcloudMetadataItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Updates the Nextcloud metadata.
 *
 * @Action(
 *   id = "nextcloud_metadata",
 *   action_label = @Translation("Update Nextcloud metadata"),
 *   type = "media"
 * )
 */
class UpdateMetadataAction extends EntityActionBase {
  /**
   * The queue factory.
   *
   * @var Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;


  /**
   * Queue worker plugin manager.
   *
   * @var Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a UpdateMetadataAction object.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, QueueFactory $queue_factory, QueueWorkerManagerInterface $queue_worker_manager, LoggerChannelFactoryInterface $logger_factory, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->queueFactory = $queue_factory;
    $this->queueWorkerManager = $queue_worker_manager;
    $this->loggerFactory = $logger_factory;
    $this->logger = $this->loggerFactory->get('nextcloud_dam');
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('queue'),
      $container->get('plugin.manager.queue_worker'),
      $container->get('logger.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $job = [
      'uid' => $this->currentUser->id(),
      'media_id' => $entity->id(),
      'asset_id' => $entity->field_nextcloud_id->value,
    ];
    $this->queueFactory->get('nextcloud_dam_update_metadata')->createItem($job);
    // @todo settings about run immediate or wait for cron
    if (TRUE) {
      // $entity->getSource()->ensureMetadata($entity, TRUE);
      /**
       * @var QueueInterface $queue
       */
      $queue = $this->queueFactory->get('nextcloud_dam_update_metadata');
      /**
       * @var QueueWorkerInterface $queue_worker
       */
      $queue_worker = $this->queueWorkerManager->createInstance('nextcloud_dam_update_metadata');

      while ($item = $queue->claimItem()) {
        try {
          $queue_worker->processItem($item->data);
          $queue->deleteItem($item);
        }
        catch (NextcloudServerDefinitionException $e) {
          Error::logException($this->logger, $e);

          $queue->deleteItem($item);
        }
        catch (SuspendQueueException $e) {
          $queue->releaseItem($item);
          break;
        }
        catch (\Exception $e) {
          Error::logException($this->logger, $e);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (!$object->hasField(NextcloudMetadataItem::METADATA_FIELD_NAME) || $object->get(NextcloudMetadataItem::METADATA_FIELD_NAME)->getFieldDefinition()->getType() !== 'nextcloud_metadata') {
      return $return_as_object ? AccessResult::forbidden() : FALSE;
    }

    return $return_as_object ? AccessResult::allowed() : TRUE;
  }

}
