<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Template\Attribute;

/**
 * Plugin implementation of the 'nextcloud_metadata' formatter.
 *
 * @FieldFormatter(
 *   id = "nextcloud_metadata",
 *   label = @Translation("Nextcloud metadata"),
 *   field_types = {
 *     "nextcloud_metadata"
 *   }
 * )
 */
class NextcloudMetadataFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $metadata = $this->datalistItemsFromJson(json_decode($item->value));
      $tooltip_attributes = new Attribute();
      $tooltip_attributes->setAttribute('role', 'tooltip')
        ->setAttribute('class', ['nextcloud_dam-tooltip']);
      $elements[$delta]['tooltip'] = [
        '#title' => $this->t('info'),
        '#title_attributes' => new Attribute(),
        '#theme' => 'nextcloud_metadata',
        '#tooltip_attributes' => $tooltip_attributes,
        '#metadata_data_list' => $metadata,
        '#raw' => $item->value,
        '#raw_attributes' => new Attribute(),
      ];
    }

    return $elements;
  }

  /**
   * Converts json to descriptive list.
   *
   * @var array $assoc_array
   *   The associative array to be used as descriptive list
   *
   * @return array
   *   The descriptive list render array.
   */
  public static function datalistItemsFromJson($assoc_array) {
    $wrapper = [];
    foreach ($assoc_array as $key => $value) {
      $wrapper[$key] = ['#markup' => $value];
    }
    return $wrapper;
  }

}
