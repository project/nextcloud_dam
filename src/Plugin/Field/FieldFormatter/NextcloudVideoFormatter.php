<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Template\Attribute;
use Drupal\nextcloud_dam\Plugin\media\Source\Nextcloud;

/**
 * Plugin implementation of the 'Nextcloud Video' formatter.
 *
 * @FieldFormatter(
 *   id = "nextcloud_video",
 *   label = @Translation("Nextcloud (Video)"),
 *   field_types = {"string", "string_long"}
 * )
 */
class NextcloudVideoFormatter extends NextcloudFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'controls' => TRUE,
      'autoplay' => FALSE,
      'loop' => FALSE,
      'muted' => FALSE,
      'width' => 640,
      'height' => 480,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $elements['controls'] = [
      '#title' => $this->t('Show playback controls'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('controls'),
    ];
    $elements['autoplay'] = [
      '#title' => $this->t('Autoplay'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('autoplay'),
    ];
    $elements['loop'] = [
      '#title' => $this->t('Loop'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('loop'),
    ];
    $elements['muted'] = [
      '#title' => $this->t('Muted'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('muted'),
    ];
    $elements['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->getSetting('width'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
      '#required' => TRUE,
    ];
    $elements['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->getSetting('height'),
      '#size' => 5,
      '#maxlength' => 5,
      '#field_suffix' => $this->t('pixels'),
      '#min' => 0,
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Controls: %controls', ['%controls' => $this->getSetting('controls') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Autoplay: %autoplay', ['%autoplay' => $this->getSetting('autoplay') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Loop: %loop', ['%loop' => $this->getSetting('loop') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Muted: %muted', ['%muted' => $this->getSetting('muted') ? $this->t('yes') : $this->t('no')]);
    $summary[] = $this->t('Size: %width x %height pixels', [
      '%width' => $this->getSetting('width'),
      '%height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $is_entityreference = $this->fieldDefinition->getType() == 'entity_reference';

    foreach ($items as $item) {

      /** @var \Drupal\media\MediaInterface $media */
      $media = $is_entityreference ? $item->entity : $items->getEntity();
      if (!$media) {
        continue;
      }
      $source_plugin = $media->getSource();

      if ($source_plugin instanceof Nextcloud && ($video_preview_urls = $source_plugin->getMetadata($media, 'url') . '/download/' . $source_plugin->getMetadata($media, 'file_target'))) {
        $attributes = new Attribute();
        $attributes->setAttribute('controls', $this->getSetting('controls'))
          ->setAttribute('autoplay', $this->getSetting('autoplay'))
          ->setAttribute('loop', $this->getSetting('loop'))
          ->setAttribute('muted', $this->getSetting('muted'))
          ->setAttribute('width', $this->getSetting('width'))
          ->setAttribute('height', $this->getSetting('height'));
        $source_attributes = [];
        foreach ([$video_preview_urls] as $video_url) {
          $source_attribute = new Attribute();

          $source_attribute->setAttribute('src', $video_url);

          $source_attribute->setAttribute('type', $source_plugin->getMetadata($media, 'mimetype'));
          $source_attributes[] = $source_attribute;
        }
        $elements[] = [
          '#theme' => 'nextcloud_video',
          '#attributes' => $attributes,
          '#source_attributes' => $source_attributes,
        ];
      }
    }

    return $elements;
  }

}
