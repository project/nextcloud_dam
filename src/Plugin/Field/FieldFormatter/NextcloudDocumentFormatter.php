<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\nextcloud_dam\Plugin\media\Source\Nextcloud;

/**
 * Plugin implementation of the 'Nextcloud Document' formatter.
 *
 * @FieldFormatter(
 *   id = "nextcloud_document",
 *   label = @Translation("Nextcloud (Document)"),
 *   field_types = {"string", "string_long", "entity_reference"}
 * )
 */
class NextcloudDocumentFormatter extends NextcloudFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link' => TRUE,
      'title_field' => '',
      'direct_link' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Render as link'),
      '#description' => $this->t('Whether the URL should be shown as a link or just as a plain URL.'),
      '#default_value' => $this->getSetting('link'),
    ];

    $field_candidates = $this->getFieldAndMetadataCandidates();
    $elements['title_field'] = [
      '#type' => 'select',
      '#options' => $field_candidates,
      '#title' => $this->t('Link Title field'),
      '#description' => $this->t('Select the name of the field that should be used for the link title. Falls back to the name of the file if not set.'),
      '#default_value' => $this->getSetting('title_field'),
      '#empty_option' => $this->t('- File name -'),
    ];

    $elements['direct_link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use direct link'),
      '#description' => $this->t('Nextcloud offers two class of links, the share link that redirects to nextcloud instance or direct download link.'),
      '#default_value' => $this->getSetting('direct_link'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();
    $summary[] = $this->t('Link: @link', ['@link' => $settings['link'] ? $this->t('Yes') : $this->t('No')]);

    if ($settings['link']) {
      $field_candidates = $this->getFieldAndMetadataCandidates(FALSE);
      $summary[] = $this->t('Link title field: @field', ['@field' => $settings['title_field'] ? $field_candidates[$settings['title_field']] : $this->t('- File name -')]);
      $summary[] = $this->t('Use direct link: @direct_link', ['@direct_link' => $settings['direct_link'] ? $this->t('Yes') : $this->t('No')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $is_entityreference = $this->fieldDefinition->getType() == 'entity_reference';

    foreach ($items as $delta => $item) {

      /** @var \Drupal\media\MediaInterface $media */
      $media = $is_entityreference ? $item->entity : $items->getEntity();
      if (!$media) {
        continue;
      }
      $source_plugin = $media->getSource();

      if ($source_plugin instanceof Nextcloud && ($share_url = $source_plugin->getMetadata($media, 'url'))) {
        if ($this->getSetting('link')) {
          $url = ($this->getSetting('direct_link')) ? $share_url : "{$share_url}/download{$source_plugin->getMetadata($media, 'file_target')}";

          if ($this->getSetting('title_field')) {
            $title = $this->getValueFromFieldOrMetadata($media, $this->getSetting('title_field'));
            if ($this->getSetting('title_field') === 'file_target') {
              $title = pathinfo($title)['filename'];
            }
          }
          else {
            $title = basename($url);
          }

          $elements[$delta] = [
            '#type' => 'link',
            '#title' => $title,
            '#url' => Url::fromUri($url),
          ];
        }
        else {
          $elements[$delta] = [
            '#plain_text' => $url,
          ];
        }
      }
    }

    return $elements;
  }

}
