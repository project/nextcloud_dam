<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\nextcloud_dam\Plugin\media\Source\Nextcloud;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Nextcloud' formatter.
 *
 * @FieldFormatter(
 *   id = "nextcloud_image",
 *   label = @Translation("Nextcloud (PublicPreview)"),
 *   field_types = {"string", "string_long", "entity_reference"}
 * )
 */
class NextcloudImageFormatter extends NextcloudFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityRepository = $container->get('entity.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'remote_query' => '',
      'alt_field' => '',
      'title_field' => '',
      'responsive_fallback_query' => '',
      'responsive_sizes' => '',
      'responsive_srcset_query' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    try {
      $derivatives = [
        'mini' => 'x=80&y=80&a=1',
        'webimage' => 'x=800&y=600&a=1',
        'thul' => 'x=250&y=250&a=1',
        'responsive' => 'Responsive',
      ];
      $derivatives_options = array_map(function ($key, $item) {
        return [
          '#type' => 'html_tag',
          '#tag' => 'option',
          '#value' => $key,
          '#attributes' => ['value' => $item],
        ];
        // Return "<option value='$item'>$key</option>";.
      }, array_keys($derivatives), array_values($derivatives));

    }
    catch (ConnectException $e) {
      $derivatives = [];
    }

    $elements['remote_query'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preset'),
      '#description' => $this->t('Select the preset name (click when empty) or enter manually the properties to be used to display the image as : ?x=250&y=250&a=1 for example. Enter Responsive to set responsive instead.'),
      '#default_value' => $this->getSetting('remote_query'),
      '#attributes' => ['class' => ['nextcloud-derivative'], 'list' => 'thumb_datalist'],
    ];
    $elements['thumbnail_datalist'] = [
      '#type' => 'html_tag',
      '#tag' => 'datalist',
      '#attributes' => ['id' => 'thumb_datalist'],
      'options' => $derivatives_options,
    ];
    $elements['responsive_datalist'] = [
      '#type' => 'html_tag',
      '#tag' => 'datalist',
      '#attributes' => ['id' => 'responsive_datalist'],
      'options' => array_filter($derivatives_options, function ($item) {
          return $item['#value'] != 'responsive';
      }),
    ];

    $field_candidates = $this->getFieldAndMetadataCandidates();

    $elements['alt_field'] = [
      '#type' => 'select',
      '#options' => $field_candidates,
      '#title' => $this->t('Alt attribute field'),
      '#description' => $this->t('Select the name of the field that should be used for the "alt" attribute of the image.'),
      '#default_value' => $this->getSetting('alt_field'),
      '#empty_value' => '',
    ];

    $elements['title_field'] = [
      '#type' => 'select',
      '#options' => $field_candidates,
      '#title' => $this->t('Title attribute field'),
      '#description' => $this->t('Select the name of the field that should be used for the "title" attribute of the image.'),
      '#default_value' => $this->getSetting('title_field'),
      '#empty_value' => '',
    ];

    $open_api_documentation = 'https://docs.nextcloud.com/server/latest/developer_manual/_static/openapi.html#/operations/core-preview-get-preview-by-file-id';
    $elements['responsive_fallback_query'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback preset'),
      '#description' => $this->t('Select the preset name (click when empty) or enter manually the attributes that should be applied to the images. See  <a href=":open_api_help">here</a> for explanation on possible values. Should start right after the "?", e.g. "x=100&y=100&mode=fill" If the following Responsive image fields are filled, this field defines the fallback image if the responsive settings are broken.', [
        ':open_api_help' => $open_api_documentation,
      ]),
      '#attributes' => ['list' => 'responsive_datalist'],
      '#default_value' => $this->getSetting('responsive_fallback_query'),
      '#states' => [
        'visible' => [
          ':input.nextcloud-derivative' => ['value' => 'Responsive'],
        ],
        'required' => [
          ':input.nextcloud-derivative' => ['value' => 'Responsive'],
        ],
      ],
    ];

    $elements['responsive_sizes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Sizes for responsive images'),
      '#description' => $this->t('With this and the next field filled, a set of responsive images is generated. String that will be fed in the "sizes" attribute of the img object.'),
      '#default_value' => $this->getSetting('responsive_sizes'),
      '#states' => [
        'visible' => [
          ':input.nextcloud-derivative' => ['value' => 'Responsive'],
        ],
      ],
    ];

    $elements['responsive_srcset_query'] = [
      '#type' => 'textarea',
      '#title' => $this->t('The set of responsive images (srcset)'),
      '#description' => $this->t('The different images to be used in a responsive setting. It should have the form: "x=200&y=200&mode=fill 100w, x=400&y=400&mode=fill 200w, ...", with the "parameters..." following <a href=":open_api_help">these</a> guidelines, and the second argument being the width of the image. Make sure to separate each setting with ", ".', [
        ':open_api_help' => $open_api_documentation,
      ]),
      '#default_value' => $this->getSetting('responsive_srcset_query'),
      '#states' => [
        'visible' => [
          ':input.nextcloud-derivative' => ['value' => 'Responsive'],
        ],
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();

    if ($settings['remote_query'] == 'Responsive') {
      if (!empty($settings['responsive_srcset_query']) && !empty($settings['responsive_sizes'])) {
        $summary[] = $this->t('Responsive query configuration: Responsive, fallback: @responsive_fallback_query', ['@responsive_fallback_query' => $settings['responsive_fallback_query']]);
      }
      else {
        $summary[] = $this->t('Query configuration: @responsive_fallback_query', ['@responsive_fallback_query' => $settings['responsive_fallback_query']]);
      }
    }
    else {
      $summary[] = $this->t('Derivative: @style', ['@style' => $settings['remote_query']]);
    }
    $field_candidates = $this->getFieldAndMetadataCandidates(FALSE);
    if (empty($settings['title_field'])) {
      $summary[] = $this->t('Title attribute not displayed (not recommended).');
    }
    else {
      $summary[] = $this->t('Title attribute field: @field', ['@field' => $field_candidates[$settings['title_field']]]);
    }

    if (empty($settings['alt_field'])) {
      $summary[] = $this->t('Alt attribute not displayed (not recommended).');
    }
    else {
      $summary[] = $this->t('Alt attribute field: @field', ['@field' => $field_candidates[$settings['alt_field']]]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $element = [];
    $is_entityreference = $this->fieldDefinition->getType() == 'entity_reference';

    foreach ($items as $delta => $item) {
      /** @var \Drupal\media\MediaInterface $media_entity */
      if ($media_entity = $is_entityreference ? $item->entity : $items->getEntity()) {
        /** @var \Drupal\media\MediaInterface $media_entity */
        $media_entity = $this->entityRepository->getTranslationFromContext($media_entity, $langcode);
        /** @var \Drupal\media\MediaSourceInterface $source_plugin */
        $source_plugin = $media_entity->getSource();
        if ($source_plugin instanceof Nextcloud && ($thumbnail = $source_plugin->getMetadata($media_entity, 'server') . "/index.php/apps/files_sharing/publicpreview/" . $source_plugin->getMetadata($media_entity, 'token'))) {
          $element[$delta]['nextcloud_wrapper'] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => ['nextcloud-wrapper'],
            ],
          ];

          $element[$delta]['nextcloud_wrapper']['image'] = [
            '#theme' => 'image',
            '#uri' => '',
            '#attributes' => ['class' => ['nextcloud-image']],
          ];

          if (isset($thumbnail)) {
            if ($settings['remote_query'] == 'Responsive' && !empty($settings['responsive_fallback_query'])) {
              $transform_query = $this->getSetting('responsive_fallback_query');
              // Set responsive specific attributes.
              if ($this->getSetting('remote_query') == 'Responsive' && isset($settings['responsive_srcset_query']) && isset($settings['responsive_sizes'])) {
                $responsive_srcset_query_arr = explode(' ', $settings['responsive_srcset_query']);
                $srcset = array_map(function ($item) use ($thumbnail) {
                  return $thumbnail . '?' . $item;
                }, $responsive_srcset_query_arr);
                $element[$delta]['nextcloud_wrapper']['image']['#attributes']['srcset'] = implode(" ", $srcset);
                $element[$delta]['nextcloud_wrapper']['image']['#attributes']['sizes'] = $settings['responsive_sizes'];
              }
            }
            else {
              $transform_query = $settings['remote_query'];
            }
          }

          $element[$delta]['nextcloud_wrapper']['image']['#uri'] = $thumbnail . '?' . $transform_query;

          if ($settings['title_field'] && $title = $this->getValueFromFieldOrMetadata($media_entity, $settings['title_field'])) {
            $element[$delta]['nextcloud_wrapper']['image']['#title'] = $title;
          }
          if ($settings['alt_field'] && $alt = $this->getValueFromFieldOrMetadata($media_entity, $settings['alt_field'])) {
            $element[$delta]['nextcloud_wrapper']['image']['#alt'] = $alt;
          }
          $this->renderer->addCacheableDependency($element[$delta]['nextcloud_wrapper']['image'], $item);
          /* $element[$delta]['nextcloud_wrapper']['tooltip'] = [
          '#type' => 'html_tag',
          '#access' => AccessResult::allowedIfHasPermission($this->currentUser,
          'view nextcloud_dam media usage'),
          '#tag' => 'div',
          '#attributes' => [
          'role' => 'tooltip',
          'class' => ['nextcloud-tooltip'],
          ],
          '#value' => $this->t('Media description: @description', [
          '@description' => $this->getValueFromFieldOrMetadata($media_entity,
          'nextcloud_metadata')]),
          ];
          $this->renderer->addCacheableDependency($element[$delta]['nextcloud_wrapper']['tooltip'],
          $item);
           */
        }
      }
    }

    return $element;
  }

}
