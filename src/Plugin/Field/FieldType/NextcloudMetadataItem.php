<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'nextcloud_metadata' field type.
 *
 * @FieldType(
 *   id = "nextcloud_metadata",
 *   label = @Translation("Nextcloud metadata"),
 *   description = @Translation("This field stores a JSON object that describes metadata fetched from the Nextcloud API."),
 *   category = @Translation("Nextcloud"),
 *   default_widget = "nextcloud_metadata",
 *   default_formatter = "nextcloud_metadata",
 *   no_ui = TRUE,
 * )
 */
class NextcloudMetadataItem extends FieldItemBase {

  /**
   * The Nextcloud metadata field name.
   */
  const METADATA_FIELD_NAME = 'nextcloud_metadata';

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')->setLabel(t('JSON Value'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema['columns']['value']['type'] = 'text';
    $schema['columns']['value']['size'] = 'normal';

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->value === NULL || $this->value === '';
  }

}
