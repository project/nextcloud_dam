<?php

namespace Drupal\nextcloud_dam\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'nextcloud_metadata' widget.
 *
 * @FieldWidget(
 *   id = "nextcloud_metadata",
 *   label = @Translation("Nextcloud metadata"),
 *   field_types = {
 *     "nextcloud_metadata"
 *   },
 * )
 */
class NextcloudMetadataWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fetch metadata'),
    ];
    return $element;
  }

}
