<?php

namespace Drupal\nextcloud_dam\Plugin\EntityBrowser\Widget;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\entity_browser\WidgetBase;
use Drupal\entity_browser\WidgetValidationManager;
use Drupal\nextcloud_dam\Exception\BundleNotExistException;
use Drupal\nextcloud_dam\Exception\BundleNotNextcloudException;
use Drupal\nextcloud_dam\NextcloudDamApiInterface;
use Drupal\nextcloud_dam\NextcloudDamAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base class for NextcloudDam Entity browser widgets.
 */
abstract class NextcloudDamWidgetBase extends WidgetBase {

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * NextcloudDam API service.
   *
   * @var \Drupal\nextcloud_dam\NextcloudDamApiInterface
   */
  protected $nextcloudDamApi;

  /**
   * NextcloudDam Auth service.
   *
   * @var \Drupal\nextcloud_dam\NextcloudDamAuthInterface
   */
  protected $nextcloudDamAuth;


  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * NextcloudDamWidgetBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\entity_browser\WidgetValidationManager $validation_manager
   *   The Widget Validation Manager service.
   * @param \Drupal\nextcloud_dam\NextcloudDamApiInterface $nextcloud_dam_api
   *   NextcloudDam API service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\nextcloud_dam\NextcloudDamAuthInterface $nextcloud_dam_auth
   *   Nextcloud authentication utils service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EventDispatcherInterface $event_dispatcher, EntityTypeManagerInterface $entity_type_manager, WidgetValidationManager $validation_manager, NextcloudDamApiInterface $nextcloud_dam_api, LoggerChannelFactoryInterface $logger_factory, LanguageManagerInterface $language_manager, RequestStack $request_stack, ConfigFactoryInterface $config_factory, AccountProxyInterface $account_proxy, NextcloudDamAuthInterface $nextcloud_dam_auth) {
    $this->nextcloudDamAuth = $nextcloud_dam_auth;
    $this->nextcloudDamApi = $nextcloud_dam_api;
    $this->loggerFactory = $logger_factory;
    $this->languageManager = $language_manager;
    $this->requestStack = $request_stack;
    $this->config = $config_factory;
    $this->accountProxy = $account_proxy;
    $this->entityTypeManager = $entity_type_manager;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $event_dispatcher, $entity_type_manager, $validation_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('event_dispatcher'),
          $container->get('entity_type.manager'),
          $container->get('plugin.manager.entity_browser.widget_validation'),
          $container->get('nextcloud_dam.api'),
          $container->get('logger.factory'),
          $container->get('language_manager'),
          $container->get('request_stack'),
          $container->get('config.factory'),
          $container->get('current_user'),
          $container->get('nextcloud_dam.auth')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'media_type' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#default_value' => $this->configuration['media_type'],
      '#required' => TRUE,
      '#options' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    // @todo add option to load system nextcloud user if choosen in admin settings.
    return $form;
  }

  /**
   * Check that media type is properly configured.
   *
   * @return bool
   *   Returns TRUE if media type is configured correctly.
   */
  protected function checkType() {
    /**
* @var \Drupal\media\MediaTypeInterface $type
*/
    $type = $this->entityTypeManager->getStorage('media_type')
      ->load($this->configuration['media_type']);

    if (!$type) {
      (new BundleNotExistException(
            $this->configuration['media_type']
        ))->logException()->displayMessage();
      return FALSE;
    }
    // @todo dedicated Nextcloud media entity.
    elseif (!($type->getSource() /*instanceof NextcloudDam*/)) {
      (new BundleNotNextcloudException($type->label()))->logException()
        ->displayMessage();
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets social auth profiles related with current user (without access_token)
   *
   * @param string $network_id
   *   Will fetch this network's social_auth.
   *
   * @return array
   *   An array of the social_auth entities
   */
  protected function socialAuths($network_id): array {
    /**
* @var \Drupal\Core\Entity\Query\QueryInterface $entity_query
*/
    $entity_query = $this->entityTypeManager->getStorage('social_auth')->getQuery();
    $entity_query
      ->condition('user_id', $this->accountProxy->id())
      ->condition('plugin_id', $network_id, 'STARTS_WITH')
      ->accessCheck(TRUE);
    $result = $entity_query->execute();
    $entities = $result ? $this->entityTypeManager->getStorage('social_auth')
      ->loadMultiple($result) : [];
    return $entities;
  }

}
