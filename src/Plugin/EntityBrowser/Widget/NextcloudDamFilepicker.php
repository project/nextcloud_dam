<?php

namespace Drupal\nextcloud_dam\Plugin\EntityBrowser\Widget;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\media\MediaInterface;
use Drupal\nextcloud_dam\Exception\BundleNotExistException;
use Drupal\nextcloud_dam\Exception\BundleNotNextcloudException;
use Drupal\nextcloud_dam\Plugin\media\Source\Nextcloud;

/**
 * Uses a Nextcloud API to search and provide entity listing using browser es.
 *
 * @EntityBrowserWidget(
 *   id = "nextcloud_dam_filepicker",
 *   label = @Translation("Nextcloud DAM filepicker"),
 *   description = @Translation("Adds a Nextcloud filepicker widget.")
 * )
 */
class NextcloudDamFilepicker extends NextcloudDamWidgetBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // media_type not suffixed by image but it is.
      'media_type' => NULL,
      'relationship' => 'link',
      'media_type_document' => NULL,
      'media_type_video' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['submit_text']['#access'] = FALSE;

    foreach ($this->entityTypeManager->getStorage('media_type')->loadMultiple() as $type) {
      /**
* @var \Drupal\media\MediaTypeInterface $type
*/
      if ($type->getSource() instanceof Nextcloud) {
        $form['media_type']['#options'][$type->id()] = $type->label();
      }
    }

    $form['media_type']['#title'] = $this->t('Media type (Image)');

    if (empty($form['media_type']['#options'])) {
      $form['media_type']['#disabled'] = TRUE;
      $form['media_type']['#description'] = $this->t(
            'You must @create_type before using this widget.', [
              '@create_type' => Link::createFromRoute($this->t('create a Nextcloud media type'), 'entity.media_type.add_form')
                ->toString(),
            ]
        );
    }
    else {

      $form['media_type']['#required'] = FALSE;
      $form['media_type']['#empty_option'] = $this->t('- Hide images -');

      $form['media_type_document'] = [
        '#type' => 'select',
        '#title' => $this->t('Media type(Document)'),
        '#default_value' => $this->configuration['media_type_document'],
        '#required' => FALSE,
        '#options' => $form['media_type']['#options'],
        '#empty_option' => $this->t('- Hide documents -'),
      ];

      $form['media_type_video'] = [
        '#type' => 'select',
        '#title' => $this->t('Media type (Video)'),
        '#default_value' => $this->configuration['media_type_video'],
        '#required' => FALSE,
        '#options' => $form['media_type']['#options'],
        '#empty_option' => $this->t('- Hide videos -'),
      ];
      $form['relationship'] = [
        '#type' => 'select',
        '#title' => $this->t('Relationship'),
        '#default_value' => $this->configuration['relationship'],
        '#options' => [
          'link' => $this->t('link'),
          'copy' => $this->t('copy'),
          'integrated' => $this->t('integrated'),
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    if (!$this->checkType()) {
      return [];
    }
    $media = [];
    $selection = Json::decode($form_state->getValue('nextcloud_selection', ''));
    $storage = $this->entityTypeManager->getStorage('media');
    if (empty($selection['shareLinks'])) {
      return [];
    }

    $image_type = NULL;
    $image_source_field = NULL;
    $source_fields = [];
    if ($this->configuration['media_type']) {
      /**
* @var \Drupal\media\MediaTypeInterface $image_type
*/
      $image_type = $this->entityTypeManager->getStorage('media_type')
        ->load($this->configuration['media_type']);
      $image_source_field = $image_type->getSource()->getConfiguration()['source_field'];
      $source_fields[] = $image_source_field;
    }

    $document_type = NULL;
    $document_source_field = NULL;
    if ($this->configuration['media_type_document']) {
      /**
* @var \Drupal\media\MediaTypeInterface $document_type
*/
      $document_type = $this->entityTypeManager->getStorage('media_type')
        ->load($this->configuration['media_type_document']);
      $document_source_field = $document_type->getSource()->getConfiguration()['source_field'];
      if ($document_source_field != $image_source_field) {
        $source_fields[] = $document_source_field;
      }
    }

    $video_type = NULL;
    $video_source_field = NULL;
    if ($this->configuration['media_type_video']) {
      /**
* @var \Drupal\media\MediaTypeInterface $video_type
*/
      $video_type = $this->entityTypeManager->getStorage('media_type')
        ->load($this->configuration['media_type_video']);
      $video_source_field = $video_type->getSource()->getConfiguration()['source_field'];
      if ($video_source_field != $image_source_field) {
        $source_fields[] = $video_source_field;
      }
    }

    foreach ($selection['shareLinks'] as $nextcloud_info) {
      $query = $storage->getQuery();
      $query->accessCheck(FALSE);

      $source_field_condition = $query->orConditionGroup();
      foreach ($source_fields as $source_field) {
        // Check if field_nextcloud_url matches this info url, to reuse it.
        $source_field_condition->condition($source_field, $nextcloud_info['data']['url']);
      }

      $mid = $query
        ->condition($source_field_condition)
        ->range(0, 1)
        ->execute();
      if ($mid) {
        $media[] = $storage->load(reset($mid));
      }
      elseif ($nextcloud_info['data']['item_type'] == 'file') {
        if (substr($nextcloud_info['data']['mimetype'], 0, 5) == 'image' && $image_type) {
          // As Filepicker client already provides the metadata fill it now.
          $image_media = $storage->create(
                [
                  'bundle' => $image_type->id(),
                  $image_source_field => $nextcloud_info['data']['url'],
                  'name' => $nextcloud_info['data']['label'],
                ]
            );
          $image_media->getSource()->fillMetadata($image_media, $nextcloud_info['data'], $this->configuration['relationship']);
          $media[] = $image_media;

        }
        elseif (substr($nextcloud_info['data']['mimetype'], 0, 5) == 'video' && $video_type) {
          $video_media = $storage->create(
                [
                  'bundle' => $video_type->id(),
                  $video_source_field => $nextcloud_info['data']['url'],
                  'name' => $nextcloud_info['data']['label'],
                ]
            );
          $video_media->getSource()->fillMetadata($video_media, $nextcloud_info['data'], $this->configuration['relationship']);
          $media[] = $video_media;
        }
        elseif ($document_type) {
          $document_media = $storage->create(
                [
                  'bundle' => $document_type->id(),
                  $document_source_field => $nextcloud_info['data']['url'],
                  'name' => ltrim($nextcloud_info['data']['file_target'], '/'),
                ]
            );
          $document_media->getSource()->fillMetadata($document_media, $nextcloud_info['data'], $this->configuration['relationship']);
          $media[] = $document_media;

        }
      }
    }
    return $media;
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);

    if ($form_state->getValue('errors')) {
      $form['actions']['submit']['#access'] = FALSE;
      return $form;
    }

    $form['nextcloud_selection'] = [
      '#type' => 'hidden',
      '#weight' => -1,
    ];
    $form['results'] = [
      '#type' => 'container',
      '#weight' => -1,
      '#attributes' => [
        'id' => ['results'],
      ],
    ];

    $nc_accounts = $this->nextcloudDamAuth->get();
    $form['vertical_tabs'] = [
      '#type' => 'fieldset',
    ];
    foreach ($nc_accounts as $network_id => $credentials) {
      $label = $credentials['network']->getPluginDefinition()['social_network'];
      $form[$network_id] = [
        "#type" => 'details',
        '#title' => $label,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#group' => 'vertical_tabs',
      ];
      $social_auth = array_values($this->socialAuths($network_id));
      if ($social_auth = array_shift($social_auth)) {
        // Expired or non existing access_token, but it also could be
        // rejected in Nextcloud so it must appear login button also
        // then (it needs via not connected in js side)
        $credentials_expire_time = ($credentials['access_token']) ? $credentials['access_token']->getExpires() : 0;
        if ($credentials_expire_time < (new DrupalDateTime())->getTimestamp()) {
          /* This could refresh but refresh is too short in nextcloud
           * to run this risky code. Better force manual
          try {
          // Drupal\social_auth\Plugin\Network\NetworkInterface
          $sdk = $credentials["network"]->getSdk();
          $newAccessToken = $sdk->getAccessToken('refresh_token', [
          'refresh_token' => $credentials['access_token']->getRefreshToken()
          ]);
          }
          catch (Exception $e) {
          $newAccessToken = False;
          }
          if (!$newAccessToken){
          $needs_login_network = $credentials['network'];
          continue;
          }
           */

          /*$form[$network_id][] =
          [
          '#theme' => 'login_with',
          '#networks' => [$credentials['network']],
          '#destination' => Url::fromRoute('<current>',[],[])->toString(),
          ];*/

          // Next block is to enter in polling state, so when
          // user click the nextcloud link to authenticate, it will
          // poll every 5 seconds,(pressing via js a check
          // connection button) to check if it has a valid token.
          // If yes iframe page will reload and will unfold
          // the picker for nextcloud whose has a valid token.
          $attributes = [
            'target' => '_blank',
            'id' => 'nc-dam-may-authenticate',
            'data-network' => $network_id,
          ];

          $form[$network_id]['connect_information'] = [
            '#markup' => $this->t(
              'You must authenticate in @auth_link previously before using this widget. Return here after done.', [
                '@auth_link' => Link::fromTextAndUrl('this nextcloud', $credentials['network']->getRedirectUrl([
                  'attributes' => $attributes,
                  'query' => [
                    // @todo add succesfully connected page
                    'destination' => Url::fromRoute('nextcloud_dam.succesful_login', [], [])->toString(),
                  ],
                ]))->toString(),
              ]
            ),
          ];
          $form[$network_id]['#attached']['library'][] = 'nextcloud_dam/connect-check';

          $form[$network_id]['check_connection'] = [
            '#type' => 'button',
            '#value' => $this->t('Check connection'),
            '#ajax' => [
              'callback' => [$this, 'checkNetworkConnectionCallback'],
              'event' => 'click',
              'progress' => [
                'type' => 'throbber',
                'message' => $this->t('Checking if connected'),
              ],
            ],
          ];

          $form[$network_id]['nc_dam_may_authenticate_result'] = [
            '#type' => 'container',
            '#attributes' => [
              'name' => 'nc-dam-may-authenticate-result',
            ],
          ];
          continue;
        }
        // As it has valid token uncollapse.
        $form[$network_id]['#open'] = TRUE;

        $form[$network_id]['#attached']['library'][] = 'nextcloud_dam/filepicker';
        $nc_url = str_replace("social_auth_nextcloud:", "https://", $network_id);
        // Set the share label.
        $original_path = $this->requestStack->getCurrentRequest()->query->get('original_path');
        $parent_entity_edit_url = Url::fromUri('internal:' . $original_path);
        if ($original_path && $parent_entity_edit_url->getRouteName() === 'entity.node.edit_form') {
          foreach ($parent_entity_edit_url->getRouteParameters() as $entity_type => $entity_id) {
            // @todo may break if entity has not canonical route or parameters doesn't match
            $main_entity_url = Url::fromRoute('entity.' . $entity_type . '.canonical');
            $main_entity_url->setRouteParameter('node', $entity_id);
            $main_entity_url->setAbsolute();
            $main_entity_url_string = $main_entity_url->toString();
          }
          $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['content_referral'] = $main_entity_url_string;
        }
        else {
          // No node editor context this could be when using standalone browser.
          $current_path_url_string = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
          $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['content_referral'] = $current_path_url_string;
        }
        $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['domain'] = $nc_url;
        $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['user'] = $social_auth->provider_user_id->value;

        // To be used whenever accessToken could be refreshed without going to
        // other side url.
        // $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']
        // ['key'] = $newAccessToken ? :
        // $credentials['access_token']->getToken();
        // $nc_account->token->value;
        $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['key'] = $credentials['access_token']->getToken();

        $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['types'] = [];

        if ($this->configuration['media_type']) {
          $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['types'][] = 'image';
        }
        if ($this->configuration['media_type_document']) {
          $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['types'][] = 'document';
        }
        if ($this->configuration['media_type_video']) {
          $form[$network_id]['#attached']['drupalSettings']['nextcloud_dam']['types'][] = 'video';
        }

        $form[$network_id]['actions']['submit']['#attributes']['class'][] = 'js-hide';

        $form[$network_id]['browser']['#markup'] = Markup::create('<div style="position: fixed; top: 44px; left: 0; right: 0; bottom: 0;" id="nextcloud_dam_mount_point"><div style="display: flex; height: 100%;"></div></div>');

      }
      else {
        $options = ['absolute' => TRUE, 'attributes' => ['target' => '_blank']];
        $form[$network_id][]['#markup'] = $this->t(
              'You must authenticate previously in @user_edit_form before using this widget.', [
                '@user_edit_form' => Link::createFromRoute($this->t('user edit form'), 'user.edit', [], $options)
                  ->toString(),
              ]
          );
      }
    }
    // Hide submit as it must be there but will submit from filebrowser.
    $form['actions']['submit']['#attributes']['class'][] = 'visually-hidden';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getTriggeringElement()['#eb_widget_main_submit'])) {
      try {
        $media = $this->prepareEntities($form, $form_state);
        array_walk(
              $media, function (MediaInterface $media_item) {
                  $media_item->save();
              }
          );
        $this->selectEntities($media, $form_state);
      }
      catch (\UnexpectedValueException $e) {
        $this->messenger()->addError($this->t('Nextcloud DAM integration is not configured correctly. Please contact the site administrator.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkType() {
    if (parent::checkType()) {
      return TRUE;
    }
    if ($this->configuration['media_type_document']) {
      /**
* @var \Drupal\media\MediaTypeInterface $type
*/
      $type = $this->entityTypeManager->getStorage('media_type')
        ->load($this->configuration['media_type_document']);

      if (!$type) {
        (new BundleNotExistException(
              $this->configuration['media_type_document']
          ))->logException()->displayMessage();
        return FALSE;
      }
      elseif (!($type->getSource() instanceof Nextcloud)) {
        (new BundleNotNextcloudException($type->label()))->logException()
          ->displayMessage();
        return FALSE;
      }
      return TRUE;
    }
    if ($this->configuration['media_type_video']) {
      /**
* @var \Drupal\media\MediaTypeInterface $type
*/
      $type = $this->entityTypeManager->getStorage('media_type')
        ->load($this->configuration['media_type_video']);

      if (!$type) {
        (new BundleNotExistException(
              $this->configuration['media_type_video']
          ))->logException()->displayMessage();
        return FALSE;
      }
      elseif (!($type->getSource() instanceof Nextcloud)) {
        (new BundleNotNextcloudException($type->label()))->logException()
          ->displayMessage();
        return FALSE;
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Ajax callback to check if token is expired.
   */
  public function checkNetworkConnectionCallback() {
    $nc_accounts = $this->nextcloudDamAuth->get();
    $ajax_response = new AjaxResponse();
    foreach ($nc_accounts as $network_id => $credentials) {
      $social_auth = array_values($this->socialAuths($network_id));
      if ($social_auth = array_shift($social_auth)) {
        // Expired or non existing access_token, but it also could be
        // rejected in Nextcloud so it must appear login button also
        // then (it needs via not connected in js side)
        if ($credentials['access_token']->getExpires() < (new DrupalDateTime())->getTimestamp()) {
          $ajax_response->addCommand(new HtmlCommand('[name=nc-dam-may-authenticate-result]', '<em>' . $this->t('Authorization token still expired') . '</em>'));
        }
        else {
          $currentURL = Url::fromRoute('<current>');
          $ajax_response->addCommand(new RedirectCommand($currentURL->toString()));
        }
      }
      return $ajax_response;

    }
  }

}
