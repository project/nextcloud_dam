<?php

namespace Drupal\nextcloud_dam\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nextcloud_dam\Exception\NextcloudConnectException;
use Drupal\nextcloud_dam\Exception\NextcloudServerDefinitionException;
use Drupal\nextcloud_dam\NextcloudDamApiInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines 'nextcloud_dam_update_metadata' queue worker.
 *
 * @QueueWorker(
 *   id = "nextcloud_dam_update_metadata",
 *   title = @Translation("Update metadata"),
 *   cron = {"time" = 30}
 * )
 */
class UpdateMetadata extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;
  /**
   * Drupal entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Time interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Cache tag invalidator service.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagInvalidator;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Nextcloud DAM api service.
   *
   * @var \Drupal\nextcloud_dam\NextcloudDamApiInterface
   *   Nextcloud DAM api service.
   */
  protected $nextcloudDam;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * NextcloudDamUpdateMetadata constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\nextcloud_dam\NextcloudDamApiInterface $nextcloud_dam
   *   The Nextcloud DAM API service.
   * @param \Drupal\Core\Logger\LoggerChannel $logger_channel
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time object.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tag_invalidator
   *   The cache tag invalidator.
   * @param Symfony\Component\EventDispatcher\EventDispatcher $event_dispatcher
   *   The dispatcher of events.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    NextcloudDamApiInterface $nextcloud_dam,
    LoggerChannelInterface $logger_channel,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    CacheTagsInvalidatorInterface $cache_tag_invalidator,
    EventDispatcherInterface $event_dispatcher,
    MessengerInterface $messenger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerChannel = $logger_channel;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->cacheTagInvalidator = $cache_tag_invalidator;
    $this->eventDispatcher = $event_dispatcher;
    $this->nextcloudDam = $nextcloud_dam;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('nextcloud_dam.api'),
          $container->get('logger.factory')->get('nextcloud_dam.update_assets'),
          $container->get('entity_type.manager'),
          $container->get('datetime.time'),
          $container->get('cache_tags.invalidator'),
          $container->get('event_dispatcher'),
          $container->get('messenger')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data['uid'] || empty($data['media_id']) || empty($data['asset_id']))) {
      return FALSE;
    }

    // A user from which get the auth token is the logged in user.
    // So allow user to go ahead with the metadata update.
    // Let's delegate this decision to nextcloud,
    // if not denied from nextcloud continue.
    $this->cacheTagInvalidator->invalidateTags(["nextcloud-dam-media:{$data['media_id']}"]);

    $media_storage = $this->entityTypeManager->getStorage('media');
    /**
* @var \Drupal\media\MediaInterface $media
*/
    $media = $media_storage->load($data['media_id']);

    if (!$media) {
      $this->loggerChannel->warning(
            'Media item (id: %media_id) associated with Dam asset (asset id: %asset_id) has been deleted.', [
              '%asset_id' => $data['asset_id'],
              '%media_id' => $data['media_id'],
            ]
        );
      return TRUE;
    }

    $server = $media->getSource()->getMetadata($media, 'server');

    try {
      $asset_data = $this->nextcloudDam->setClient($server)->getShareById($data['asset_id']);

      // If no exists.
      if ($asset_data['ocs']['meta']['statuscode'] == 404) {
        // @todo THROW  notfound
        return FALSE;
      }
      // If exists.
      if ($asset_data['ocs']['meta']['statuscode'] <= 200) {
        // Go ahead saving.
        $metadata = $asset_data['ocs']['data'][0];
      }

      // @todo get relationship from settings $this->configuration['relationship']);
      $media->getSource()->fillMetadata($media, $metadata, 'link');
      // $media->updateQueuedThumbnail();
      $media->setChangedTime($this->time->getCurrentTime());
      $media->save();
    }
    catch (ClientException $exception) {
      $response = $exception->getResponse();
      $not_existing_metadata = JSON::decode((string) $response->getBody()->getContents());
      $not_existing_metadata['ocs']['data'][0]['id'] = $data['asset_id'];
      $media->getSource()->fillMetadata($media, $not_existing_metadata, 'link');
      $media->setChangedTime($this->time->getCurrentTime());
      $media->save();
    }
    catch (\Exception $exception) {
      $this->loggerChannel->warning(
            'Cannot get asset from API. Asset id: %asset_id, error: %message',
            [
              '%asset_id' => $data['asset_id'],
              '%message' => $exception->getMessage(),
            ]
        );
      $this->messenger->addWarning(
            $this->t(
                'Cannot get asset from API. Asset id: %asset_id, error: %message',
                [
                  '%asset_id' => $data['asset_id'],
                  '%message' => $exception->getMessage(),
                ]
            )
        );
      $this->processException($exception);
    }

  }

  /**
   * Process Nextcloud DAM exceptions.
   *
   * @param \Exception $exception
   *   Exception thrown during API request.
   */
  protected function processException(\Exception $exception): void {
    switch (TRUE) {
      case $exception instanceof NextcloudServerDefinitionException:
        throw $exception;

      case $exception instanceof NextcloudConnectException && $exception->getCode() === 401:
      case $exception->getMessage() === 'Cannot get Nextcloud token for client instantiation.':
        throw new SuspendQueueException(
            $exception->getMessage(),
            $exception->getCode(),
            $exception
        );

      case $exception instanceof NextcloudConnectException && $exception->getCode() === 408:
        throw new DelayedRequeueException(60, 'Timed out loading asset, trying again later.', 408, $exception);

      case $exception instanceof NextcloudConnectException:
        throw new DelayedRequeueException(60, 'Unable to complete network request.', $exception->getCode(), $exception);

      default:
        // Try again for unknown 4xx responses.
        throw new RequeueException();
    }
  }

}
