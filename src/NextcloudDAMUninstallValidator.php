<?php

namespace Drupal\nextcloud_dam;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Helps on how to uninstall nextcloud_metadata field storage.
 */
class NextcloudDAMUninstallValidator implements ModuleUninstallValidatorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new NextcloudDAMUninstallValidator.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_manager;
    $this->stringTranslation = $string_translation;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];
    if ($module === "nextcloud_dam") {
      $this->moduleHandler->loadInclude('nextcloud_dam', 'install');
      // nextcloud_dam_uninstall();
      $requirements = nextcloud_dam_requirements("runtime");
      if ($requirements) {
        foreach ($requirements as $requirement) {
          if ($requirement['uninstallable']) {
            $reasons[] = $requirement['value'];
          }
        }
      }
    }

    return $reasons;
  }

}
