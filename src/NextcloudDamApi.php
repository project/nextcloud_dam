<?php

namespace Drupal\nextcloud_dam;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
// Group functionality.
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nextcloud_dam\Exception\NextcloudConnectException;
use Drupal\nextcloud_dam\Exception\NextcloudServerDefinitionException;

/**
 * Performs calls to nextcloud and performs nextcloud related tasks.
 */
class NextcloudDamApi implements NextcloudDamApiInterface {
  use StringTranslationTrait;
  /**
   * The Nextcloud share types.
   */
  const SHARE_GROUP = 1;
  const SHARE_PUBLIC_LINK = 3;
  const SHARE_USER = 0;

  /**
   * The http client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * The GuzzleHttp client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The server to communicate to.
   *
   * @var string
   */
  protected $server;
  /**
   * Drupal\nextcloud_dam\NextcloudDamAuthInterface definition.
   *
   * @var \Drupal\nextcloud_dam\NextcloudDamAuthInterface
   */
  protected $nextcloudDamAuth;

  /**
   * Account proxy.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $accountProxy;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * Drupal logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Constructs a new NextcloudDamApi object.
   *
   * @param \Drupal\nextcloud_dam\NextcloudDamAuthInterface $nextcloud_dam_auth
   *   The Nextcloud auth service.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   Factory to forge http client.
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   Account proxy.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The route match service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(NextcloudDamAuthInterface $nextcloud_dam_auth, ClientFactory $http_client_factory, AccountProxyInterface $account_proxy, MessengerInterface $messenger, EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $route_match, LoggerChannelFactoryInterface $logger_factory) {
    $this->accountProxy = $account_proxy;
    $this->nextcloudDamAuth = $nextcloud_dam_auth;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->clientFactory = $http_client_factory;
    $this->loggerFactory = $logger_factory;
    $this->loggerChannel = $this->loggerFactory->get('nextcloud_dam.update_assets');
  }

  /**
   * {@inheritdoc}
   */
  public function setClient($server, $uid = FALSE, $maintainer = FALSE) {
    if ($maintainer) {
      $this->loggerChannel->warning('Performing tasks as maintainer is not yet implemented');
    }
    else {
      // Logged in user client or passed by reference.
      $user = $uid ? $this->entityTypeManager->getStorage('user')->load($uid) : $this->entityTypeManager->getStorage('user')->load($this->accountProxy->id());
      $nc_accounts = $this->nextcloudDamAuth->get();

      if (!$server) {
        throw new NextcloudServerDefinitionException("No server defined for this asset, aborting");
      }
      $this->server = $server;
      $server_domain = parse_url($server)['host'];
      $social_auth = $this->socialAuths($server_domain, $user->id());
      if ($social_auth = array_shift($social_auth)) {
        if (!isset($nc_accounts["social_auth_nextcloud:$server_domain"])) {
          throw new NextcloudServerDefinitionException("No social auth authentication defined for server: $server");
        }
        if (!$nc_accounts["social_auth_nextcloud:$server_domain"]['access_token']) {
          throw new NextcloudConnectException("No access token request attempt has been done yet with social auth authentication defined for server: $server");
        }
        if ($nc_accounts["social_auth_nextcloud:$server_domain"]['access_token']->getExpires() < (new DrupalDateTime())->getTimestamp()) {
          throw new NextcloudConnectException("The access token has expired, need to request a new one for server: $server");
        }

        $nc_creds_key = $nc_accounts["social_auth_nextcloud:$server_domain"]['access_token']->getToken();
        $nc_creds_user = $social_auth->provider_user_id->value;
        $this->client = $this->clientFactory->fromOptions(
              [
                'base_uri' => $server,
                'auth' => [
                  $nc_creds_user,
                  $nc_creds_key,
                ],
                'headers' => [
                  'OCS-APIRequest' => 'true',
                ],
              ]
          );

      }
    }
    return $this;
  }

  /**
   * Gets social auth profiles related to current user (without access_token)
   *
   * @todo mv to nextcloudDamauth
   *
   * @param string $network_id
   *   Will fetch this network's social_auth.
   * @param int $user_id
   *   User used to fetch social auths.
   *
   * @return array
   *   An array of the social_auth entities
   */
  protected function socialAuths($network_id, $user_id): array {
    /**
* @var \Drupal\Core\Entity\Query\QueryInterface $entity_query
*/
    $entity_query = $this->entityTypeManager->getStorage('social_auth')->getQuery();
    $entity_query
      ->condition('user_id', $this->accountProxy->id())
      ->condition('plugin_id', "social_auth_nextcloud:$network_id", 'STARTS_WITH')
      ->accessCheck(TRUE);
    $result = $entity_query->execute();
    $entities = $result ? $this->entityTypeManager->getStorage('social_auth')
      ->loadMultiple($result) : [];
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function check() {
    /* @todo To wrap in a try catch block (better adapt where it's needed):
    try {
    $response = $this->client->get(
    "/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json"
    );
    $data = $response->getBody();
    return $data;
    }
    catch (RequestException | ClientException $e) {

    watchdog_exception('nextcloud_dam', $e);
    $this->messenger->addError($this->t(
    'Client error when connecting to nextcloud server: @error',
    ["@error" => $e->getMessage()]
    ));
    }*/

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * {@inheritdoc}
   */
  public function getShareById($share_id) {
    if (isset($this->client)) {
      $response = $this->client->get("/ocs/v2.php/apps/files_sharing/api/v1/shares/$share_id?format=json");
      $data = JSON::decode((string) $response->getBody());
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function shares($path = "", bool $reshares = TRUE, bool $subfiles = TRUE) {
    // Not used yet.
    if (isset($this->client)) {
      $path_parsed = "&path=$path";
      $response = $this->client->get("/ocs/v2.php/apps/files_sharing/api/v1/shares?format=json$path_parsed");
      $data = $response->getBody();
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function share($share_options) {
    // Not used yet.
    if (isset($this->client)) {
      $response = $this->client->post(
            "/ocs/v2.php/apps/files_sharing/api/v1/shares", [
              'form_params' => $share_options,
            ]
        );
      $data = $response->getBody();
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function shareByFileId($file_id) {
    // Not used yet.
    if (isset($this->client)) {
      $response = $this->client->get("/ocs/v2.php/apps/sharesquery/api/1/file/$file_id?format=json");
      $data = $response->getBody();
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteShare($share_id) {
    // Not used yet.
    if (isset($this->client)) {
      $response = $this->client->delete("/ocs/v2.php/apps/files_sharing/api/v1/shares/" . $share_id);
      $data = $response->getBody();
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function openDirectEditUrl($path) {
    // Not used yet.
    if (isset($this->client)) {
      $response = $this->client->post(
            "/ocs/v2.php/apps/files/api/v1/directEditing/open?format=json", [
              'form_params' => ["path" => $path],
            ]
        );
      $data = $response->getBody();
      return $data;
    }
    else {
      throw new NextcloudConnectException("Nextcloud DAM not defined, could not connect.");
    }
  }

}
