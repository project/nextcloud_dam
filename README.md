This module integrates Nextcloud in Drupal in a way that it could be used as a source of media.

### Features

It will work as DAM in drupal, as bynder, acquia dam, or other modules do. So a separate workgroup could work in Nextcloud, and the publisher could use the assets done in drupal side.
It's based on Media, Social auth and entity browser.
It needs the Webapppassword app in nextcloud side. I hope that soon things will work just with nextcloud core apps, using minimal nextcloud. But is not accepted yet in Nextcloud side.
The nextcloud credentials are not out of control in drupal, as it uses a dedicated vue filepicker frontend to operate with nextcloud content. As Oauth2 auth token expires everyday and one can revoke key from nextcloud I think it's fairly safe to use.
One could use this credential to update metadata, then this will be used from server  where drupal is placed. But once again is controlled by a token delivered from user that has a short expiration time.

### How it works

You can pick a nextcloud asset, the browser will indeed share the asset publicly and share object is used to construct the media. So in fact you use share assets urls as media source. This light media may be added to any content entity.

The source definition for these media type, is defined when it process it using a filepicker defined as entitybrowser; it will look for  some properties from metadata, that optionally will be mapped to media fields, What's mandatory is the url, as it will contain the asset share id and the server:
https://nextcloud.example.org/s/aabbccc => https://[server_domain_name]/s/[asset_share_id]
Entity browser config contains in `admin/config/content/entity_browser/nextcloud_filepicker/widgets` the mapping of known mimetypes to media bundle types, by now you could configure image, video, and document. In the future it will be great to move this mapping to some kind of config entities to be able to add dynamic media types, (for example audio).

### Post-Installation

This module comes with three predefined media type named nextcloud, nextcloud document and nextcloud video, also comes with an entity browser named `nextcloud filepicker` to pickup the files. One can replicate the media types found in `/admin/structure/media` in order to create another media types, for example if is wanted to have separated mime types coming from nextcloud, namely: "nextcloud editable document", "nextcloud svg" or something else.

One thing that this module doesn't do automatically is attach to a node content, so it's a required step.
An entity reference type field must be added for example in `article` must be done in `/admin/structure/types/manage/article/fields` referencing a nextcloud media type in `/admin/structure/types/manage/article/fields/node.article.field_nc_media`, and then choose in `/admin/structure/types/manage/article/form-display` entity browser widget with this widget configuration:

```
      entity_browser: nextcloud_filepicker
      field_widget_display: rendered_entity
      field_widget_edit: true
      field_widget_remove: true
      field_widget_replace: false
      open: false
      field_widget_display_settings:
        view_mode: default
      selection_mode: selection_append
```

The entity browser also comes in a simple way so in `/admin/config/content/entity_browser/nextcloud_filepicker/widgets` it is referencing just the nextcloud media type created automatically, but if other media types are created as explained previously, they can be mapped in `/admin/config/content/entity_browser/nextcloud_filepicker/widgets`, the relationship there could be linked or copied, take in account that using linked will use drupal and browser cache.

#### Formatters

Based on the media relationship (copied or linked) there are formatters that are working better than others.

The media that are linked, where source attribute is remote,  works better with formatters like video, public preview or document. Because they are designed to use the media directly from the remote url. So, for example, an article could use the media formatter thumbnail for teaser display, and may use the publicpreview with desired resolution for full content display.

In case of copy relationship, when media is copied to drupal file storage, will make sense to use the rendered entity or the thumbnail from content field, and media may use any formatter that one could use with regular managed file in drupal.

In case of using the thumbnail rather than rendered entity in content field formatter, it will use a thumbnail of 300px width that will be copied or used directly depending on thumbnail relationship in entity browser settings.

### Uninstall

To be uninstalled you must remove nextcloud media entities, remove the media type, and the field storage config must be deleted, as not everybody could run `drush config:del field.storage.media.nextcloud_metadata`, you can find a removal confirmation in status report section Nextcloud DAM or directly in module uninstall help text.

### Multiple nextcloud support

Multiple nextcloud instances could be used, this is done using nextcloud social auth module ( https://www.drupal.org/project/social_auth_nextcloud ), so user can obtain files from many nextclouds. Refer to social auth module for more information.
One thing that may be missed is the permanent token, nextcloud supports it using app passwords. But by now this is not implemented in social_auth, keep in touch with social_auth_nextcloud development to be informed when this feature is implemented. Non permanent Oauth2 token is used by now, this means that token will expire often and user will need to reauth the token going to nextcloud again. This is done in this manner as is the more secure way to associate each site in a way that server owner or site admin has limited power over your data and credentials, look at this post to understand a little more: https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/ .

In the content form, the entity browser will render as many nextcloud browser as credentials defined in user's social auth user settings.

The operations and communications with the nextcloud api are done in browser client side using vue.js filepicker (based in great work of : https://github.com/julien-nc/nextcloud-webdav-filepicker ). So no server side operations are done. Just the refetch operation is done in server-side. do not use it and remove permission `allow refresh metadata` for unwanted roles if server communication should be avoided.

### Refreshing metadata

Sometimes the file metadata is edited in nextcloud so drupal must refetch metadata. This could be done in each node going to `nextcloud media usage` tab in each node ( `/node/{nid}/nextcloud-usage` ).


### Plans without estimated date:

Extend nextcloud social auth to allow a permanent app password for communities that trust both endpoints (drupal and nextcloud sites).
Group contents interoperability, syncing nextcloud groups content and drupal groups content together with their access permissions.
Other content widgets and fields, such as document editors in drupal side.
Search frontend using nextcloud search api.


### Development information

Some developing information can be found in [DEVELOPMENT.md](DEVELOPMENT.md)

### Additional Requirements
Nextcloud with  properly configured <a href="https://apps.nextcloud.com/apps/webapppassword" rel="nofollow">webapppassword app</a> to allow
cross site scripts between domains of drupal and nextcloud, some info could be read in app repository.

### Recommended modules/libraries
TODO

### Similar projects

https://www.drupal.org/project/acquia_dam
https://www.drupal.org/project/bynder


### Supporting this Module
By now support is contributions welcome via contact


### Community Documentation
TODO
