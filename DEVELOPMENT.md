# Development information

## how it works

You can pick a nextcloud asset, the browser will indeed share the asset publicly and share object is used to construct the media. So in fact you use share assets urls as media source. This light media may be added to any content entity.

The source definition for these media type, is defined when it process it using a filepicker defined as entitybrowser; it will look for  some properties from metadata, that optionally will be mapped to media fields, What's mandatory is the url, as it will contain the asset share id and the server:
https://nextcloud.example.org/s/aabbccc => https://[server_domain_name]/s/[asset_share_id]
Entity browser config contains in `admin/config/content/entity_browser/nextcloud_filepicker/widgets` the mapping of known mimetypes to media bundle types, by now you could configure image, video, and document. In the future it will be great to move this mapping to some kind of config entities to be able to add dynamic media types, (for example audio).

While processing the metadata (in src/Plugin/media/Source/Nextcloud.php), it will end up using fillMetadata, where the found properties will be used to set the fields mapped. A dump of the metadata is also made in nextcloud_metadata field. Also a non remote metadata is forged, the server and the thumbnail_relationship. Server is important as is used to know the credentials to use to connect, it's extracted from asset url value. Also the used parameter to set asset_id in drupal side is the `url` as it contains both the server url and the share id. Ensuremetadata is used from entitybrowser and it will call fillMetadata, in case of the action that runs when refreshing metadata, fillmetadata is called directly.

As said when calling the action UpdateMetadataAction in some place (using the defined route or from bulk view in url  https://drupal.example.org/node/{node_id}/nextcloud-usage ), it will execute (by now immediately) the run of the queue `nextcloud_dam_update_metadata` to fillMetadata from a share api call to remote nextcloud for specified assets.


### Developing the nextcloud-webdav-filepicker

This is done using yarn, going to directory `assets/prod-nextcloud-webdav-filepicker/` and run yarn or npm as usual.
A precompiled minimal version of filepicker is added. As usual contributions welcome.
