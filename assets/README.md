By now some manual steps here to develop on vue side. The changes are not accepted yet on upstream . So for full functionality of filepicker these must be done manually.
As it is using a modified version of nextcloud filepicker by julien-nc https://github.com/julien-nc/nextcloud-webdav-filepicker.

This modified version comes with:

- Search options as in https://github.com/julien-nc/nextcloud-webdav-filepicker/pull/9 .
- Grid layout as in https://github.com/julien-nc/nextcloud-webdav-filepicker/pull/10 .
- useModal from branch useModal or PR https://github.com/julien-nc/nextcloud-webdav-filepicker/pull/6 , that allow using nextcloud filepicker inside an existing modal rather than creating a new one.

These MR are combined in branch https://github.com/Communia/nextcloud-webdav-filepicker/tree/drupal_nextcloud_dam-nextcloud_picker-x.x.x .

Also two more patches added in this directory named `webapppassword.patch` . These are the changes made by this patch:

- The patch that allows using the webapppassword files_sharing api endpoint rather than the nextcloud native one, to allow setting allowed origins to use these endpoints.
- The patch that allows using the webapppassword preview api endpoint rather than the nextcloud native one, to allow setting allowed origins to use these endpoints.
- Finally the injection of data to shareLinks to include the json of the share links in NcWebdavFilePicker.vue component.

The technical details and commands are explained in [rebuild with current patches or eventually develop new features] 


Once it is merged with upstream julien repo or forked definitively to communia repo this dev workflow will change.

This information could be seen also in https://github.com/Communia/nextcloud-webdav-filepicker/issues/1 .


Nothing need to be done because this drupal module comes with a filepicker built, but if you want to modify you can do it in assets directory.
Obtaining the code from https://github.com/julien-nc/nextcloud-webdav-filepicker or https://github.com/Communia/nextcloud-webdav-filepicker.git .


## Rebuild with current patches or eventually develop new features

Ideally github.com:Communia/nextcloud-webdav-filepicker.git will be synced the branch main with the upstream main branch. If not, you  could do it locally with:

```
git clone git@github.com:Communia/nextcloud-webdav-filepicker.git
git remote add upstream https://github.com/julien-nc/nextcloud-webdav-filepicker.git
git fetch upstream
git merge upstream/main
# in case of commit enter:
# git rebase upstream/main
```

After this, some branches like the grid-layout, search and useModal, can be merged, reviewing the changes in case of conflicts: 

For example to merge grid-layout:

```
git checkout grid-layout
git fetch upstream
git merge main
git rebase main
```

And follow the rebasing flow
Once it's done we could switch to others branches to merge them

When every branch rebase is done create a new branch to merge everything to a new ad-hoc branch

```
  git checkout  drupal_nextcloud_dam-nextcloud_picker-1.0.6
  
  git merge grid-layout
  git merge search
  # resolve conflicts
  git commit
  git push
```
  
Once all merges has been done 
Now we could patch the patch that covers the webapppassword endpoints.
wget "https://git.drupalcode.org/project/nextcloud_dam/-/raw/1.0.x/assets/webappendpoint.patch"


Also, some issues could be found when building the package that can be addressed with:

```diff
diff --git a/webpack.js b/webpack.js
index b3b5b11..6a3bbbc 100644
--- a/webpack.js
+++ b/webpack.js
@@ -59,5 +59,8 @@ webpackConfig.module.rules.push(
                fullySpecified: false
        }
 })
+webpackConfig.optimization = {
+       minimize: false,
+}

 module.exports = webpackConfig
```
## Installing

After building with `npm build`
when in `assets/nextcloud-webdav-filepicker`
`rm -r  ../build/nextcloud-webdav-filepicker`
`cp -r js ../build/nextcloud-webdav-filepicker` (it will overwrite the existing filepicker).


