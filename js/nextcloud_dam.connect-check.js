/**
 * @file
 */

(function ($, Drupal) {
  /**
   * Registers behaviours related to NextcloudDam for
   * a connection check.
   */
  Drupal.behaviors.NextcloudDamConnectCheck = {
    attach(context, settings) {
      const checkAuthenticated = (ctxt, sets) => {
        ctxt.getElementById('edit-check-connection').click();
      };
      context.getElementById('nc-dam-may-authenticate').onclick = () => {
        setInterval(checkAuthenticated, 5000, context, settings);
      };
    },
  };
})(jQuery, Drupal);
