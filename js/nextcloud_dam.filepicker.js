/**
 * @file
 */

(function ($, Drupal) {
  /**
   * Registers behaviours related to NextcloudDam search view widget.
   */
  Drupal.behaviors.NextcloudDamFilepicker = {
    attach(context, settings) {
      // get url values
      const uri = window.location.search.substring(1);
      const params = new URLSearchParams(uri);
      const login = params.get('login');
      if (login) {
        document.getElementById('login').value = login;
      }
      const password = params.get('password');
      if (password) {
        document.getElementById('password').value = password;
      }
      const accessToken = params.get('accessToken');
      if (accessToken) {
        document.getElementById('accessToken').value = accessToken;
      }
      const url = params.get('url');
      if (url) {
        document.getElementById('url').value = url;
      }
      const color = params.get('color');
      if (color) {
        document.getElementById('color').value = `#${color}`;
      }
      const darkmode = params.get('darkMode');
      if (darkmode) {
        document.getElementById('darkmode').checked = darkmode === '1';
      }

      const initialUrl = drupalSettings.nextcloud_dam.domain;
      const initialLogin = drupalSettings.nextcloud_dam.user;
      const initialPassword = drupalSettings.nextcloud_dam.key;
      // document.getElementById('password').value
      const initialAccessToken = ''; // document.getElementById('accessToken').value
      const initialColor = '#0082c9';
      const initialDarkMode = false; // document.getElementById('darkmode').checked

      const filepicker = window.createFilePicker('nextcloud_dam_mount_point', {
        url: initialUrl,
        login: initialLogin,
        password: initialPassword,
        accessToken: initialAccessToken,
        useCookies: false,
        themeColor: initialColor,
        darkMode: initialDarkMode,
        displayPreviews: true,
        displayQuotaRefresh: true,
        multipleDownload: true,
        multipleUpload: true,
        closeOnError: false,
        // enableGetFilesPath: true,
        // enableGetFilesLink: true,
        // enableDownloadFiles: true,
        // enableGetSaveFilePath: true,
        // enableGetUploadFileLink: true,
        // enableUploadFiles: true,
        useWebapppassword: true,
        useModal: false,

        // language: 'de',
        // language: 'de-DE',
        // oidcConfigLocation: 'http://localhost/oidc-config.json',
        // oidcConfig: {
        //     openIdConnect: {
        //         authority: 'http://localhost:8080/auth/realms/myrealm/.well-known/openid-configuration',
        //         client_id: 'superclient',
        //         client_secret: 'be4e2e3e-506e-4078-8919-5067edfa722a',
        //         popup_redirect_uri: 'http://localhost/oidc-callback.html',
        //         response_type: 'code',
        //         scope: 'openid email profile',
        //         automaticSilentRenew: true,
        //         accessTokenExpiringNotificationTime: 50,
        //     },
        // },
      });

      // monitor form value change
      /* document.getElementById('login').addEventListener('input', (e) => {
            filepicker.updateLogin(e.target.value)
        })

        document.getElementById('password').addEventListener('input', (e) => {
            filepicker.updatePassword(e.target.value)
        })

        document.getElementById('accessToken').addEventListener('input', (e) => {
            filepicker.updateAccessToken(e.target.value)
        })

        document.getElementById('url').addEventListener('input', (e) => {
            filepicker.updateUrl(e.target.value)
        })

        document.getElementById('color').addEventListener('change', (e) => {
            filepicker.setMainColor(e.target.value)
        })

        document.getElementById('darkmode').addEventListener('change', (e) => {
            filepicker.setDarkMode(e.target.checked)
        })
        document.getElementById('selectButton').addEventListener('click', (e) => {
            filepicker.getFilesPath()
        })
        */
      /*
        document.getElementById('linkButton').addEventListener('click', (e) => {
            filepicker.getFilesLink({
                expirationDate: new Date('2050-01-01'),
                protectionPassword: 'example passwd',
                allowEdition: true,
                linkLabel: 'custom link label',
            })
        })

*/
      // events coming from the file picker
      document.addEventListener('filepicker-unauthorized', (e) => {
        console.debug('file picker got an unauthorized response');
        console.debug(e.detail);
      });

      document.addEventListener('get-files-path', (e) => {
        console.debug('Picking files paths event');
        console.debug(e.detail);
        const resultsP = context.getElementsByName('nextcloud_selection');
        resultsP[0].value = JSON.stringify(e.detail.selection);
        const parentForm = context.getElementsByTagName('form');
        Object.values(parentForm).forEach((form) => form.submit());
      });

      document.addEventListener('get-save-file-path', (e) => {
        console.debug('no vue, received "get-save-file-path" event');
        console.debug(e.detail);
        document.getElementById('results').innerHTML =
          `Selected target directory: ${e.detail.path}`;
      });

      document.addEventListener('upload-path-link-generated', (e) => {
        console.debug('no vue, received "upload-path-link-generated" event');
        console.debug(e.detail);
        const resultsP = document.getElementById('results');
        resultsP.innerHTML = `File upload link in ${e.detail.targetDir}:`;
        const p = document.createElement('p');
        p.textContent = e.detail.link;
        resultsP.appendChild(p);
      });

      document.addEventListener('get-files-link', (e) => {
        console.debug('no vue, received "get-files-link" event');
        console.debug(e.detail);
        console.debug(filepicker);
        console.debug(this);
        e.detail.shareLinks.forEach((shareLink) => {
          // inject dav data to get rich information
          filepicker.currentElements.forEach((currentElement) => {
            if (currentElement.filename === shareLink.path) {
              shareLink.davdata = currentElement;
            }
          });
        });
        const resultsP = context.getElementsByName('nextcloud_selection');
        resultsP[0].value = JSON.stringify(e.detail);
        const parentForm = context.getElementsByTagName('form');
        Object.values(parentForm).forEach((form) => form.submit());
      });

      document.addEventListener('files-uploaded', (e) => {
        console.debug('no vue, received "files-uploaded" event');
        console.debug(e.detail);
        const resultsP = document.getElementById('results');
        resultsP.innerHTML = '';

        if (e.detail.successFiles.length > 0) {
          const p = document.createElement('p');
          p.textContent = `These files were uploaded in ${e.detail.targetDir}:`;
          resultsP.appendChild(p);
          e.detail.successFiles.forEach((f) => {
            const par = document.createElement('p');
            par.textContent = f.name;
            resultsP.appendChild(par);
          });
        }

        if (e.detail.errorFiles.length > 0) {
          const p = document.createElement('p');
          p.textContent = '!!! Those files could not be uploaded:';
          resultsP.appendChild(p);

          e.detail.errorFiles.forEach((f) => {
            const par = document.createElement('p');
            par.textContent = f.name;
            resultsP.appendChild(par);
          });
        }
      });

      document.addEventListener('files-downloaded', (e) => {
        console.debug('download errors');
        console.debug(e.detail.errorFilePaths);
        const files = e.detail.successFiles;
        console.debug('no vue, something was downloaded');
        const resultsP = document.getElementById('results');
        resultsP.innerHTML = 'Downloaded files: <br>';
        files.forEach((file) => {
          console.debug(`File : ${file.name}`);
          console.debug(file);
          const reader = new FileReader();
          reader.readAsText(file);
          reader.onload = () => {
            console.debug(reader.result);
            const p = document.createElement('P');
            p.textContent = `File ${file.name}: ${reader.result.slice(
              0,
              100,
            )}...`;
            resultsP.appendChild(p);
          };
          reader.onerror = () => {
            console.error('Impossible to read downloaded file');
            console.debug(reader.error);
          };
        });
      });

      /* Set new buttons as type button to prevent form submit */
      context.addEventListener('DOMNodeInserted', (e) => {
        document
          .querySelectorAll('.nextcloud-filepicker-wrapper button')
          .forEach((element) => {
            element.setAttribute('type', 'button');
          });
      });

      document.addEventListener('filepicker-manually-closed', (e) => {
        console.debug('Filepicker manually CLOSED');
      });

      if (typeof context.location !== 'undefined') {
        // On document load
        // filepicker.getFilesPath()
        // Inject the share label here (node url for reference)
        filepicker.getFilesLink({
          linkLabel: `Asset shared in: ${settings.nextcloud_dam.content_referral}`,
        });
        context
          .querySelectorAll('.nextcloud-filepicker-wrapper button')
          .forEach((element) => {
            element.setAttribute('type', 'button');
          });
      }

      $('.nextcloud-filepicker-wrapper button').on('click', function (event) {
        $(event.target).prop('disabled', true);
        const buttonText = event.target.innerText;
        $(event.target).html(
          `${buttonText} <img src="/core/misc/throbber-active.gif">`,
        );
        // $(event.target).parents('form').submit();
      });
    },
  };
})(jQuery, Drupal);
