<?php

declare(strict_types=1);

namespace Drupal\nextcloud_dam_test\HttpClientMiddleware;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;

/**
 * Intercepts API requests to Nextcloud to return mocked data.
 */
final class MockedResponseMiddleware {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructs a new MockedResponseMiddleware object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Middleware callable to handle mocked responses.
   */
  public function __invoke(): callable {
    return function (callable $handler): callable {
      return function (RequestInterface $request, array $options) use ($handler): PromiseInterface {
        if ($request->getUri()->getHost() === 'nextcloud.example.org') {
          $path = $request->getUri()->getPath();
          // Get share calling as
          // /ocs/v2.php/apps/files_sharing/api/v1/shares/$share_id?format=json.
          if (str_starts_with($path, '/ocs/v2.php/apps/files_sharing/api/v1/shares/82')) {
            $response = file_get_contents(__DIR__ . "/../../../fixtures/share_api/share-82-updated.json");
            return new FulfilledPromise(new Response(200, [], $response));
          }
          elseif (str_starts_with($path, '/ocs/v2.php/apps/files_sharing/api/v1/shares/83')) {
            $response = file_get_contents(__DIR__ . "/../../../fixtures/share_api/share-83-updated.json");
            return new FulfilledPromise(new Response(200, [], $response));
          }
          elseif (str_starts_with($path, '/ocs/v2.php/apps/files_sharing/api/v1/shares/84')) {
            $response = file_get_contents(__DIR__ . "/../../../fixtures/share_api/share-84-updated.json");
            return new FulfilledPromise(new Response(200, [], $response));
          }
          elseif (str_starts_with($path, '/ocs/v2.php/apps/files_sharing/api/v1/shares/404')) {
            $response = file_get_contents(__DIR__ . "/../../../fixtures/share_api/share-404.json");
            return new FulfilledPromise(new Response(404, [], $response));
          }
          // Remove COULD NOT BE USED AS CLIENT OF OAUTH CLIENT do no use
          // this middleware.
          elseif (str_starts_with($path, '/index.php/apps/oauth2/api/v1/token')) {
            $query = [];
            parse_str(urldecode($request->getUri()->getQuery()), $query);
            $response = [];
            return new FulfilledPromise(new Response(412, [], JSON::encode($response)));
          }
          // Remove next elseif block as token above cannot be middlewared.
          elseif (str_starts_with($path, '/index.php/apps/oauth2/authorize')) {
            $query = [];
            parse_str(urldecode($request->getUri()->getQuery()), $query);
            $redirect_url = urldecode($query['redirect_uri']) . "?code=authtoken123&state={$query['state']}";
            $response = [];
            $response['parsed_redirect_url'] = $redirect_url;
            $response['code'] = "authtoken123";
            $response['state'] = $query['state'];
            // Despite the logical response must be something like:
            // Response(302, [
            // 'Location' => $redirect_url ],
            // json_encode($query)
            // )
            // We output as json to be able to capture the code and state to
            // perform basic
            // get request in functional browsertest.
            return new FulfilledPromise(new Response(200, [], JSON::encode($response)));
          }

          if (!$request->getHeader('OCS-API-REQUEST')) {
            $response = '{"message": "CSRF check failing"}';
            return new FulfilledPromise(new Response(412, [], $response));
          }
          // Intercept calls as
          // /index.php/core/preview?fileId=$fileid&x=50&y=50&forceIcon=0&a=0.
          if (str_starts_with($path, '/index.php/core/preview')) {
            $query = [];
            parse_str(urldecode($request->getUri()->getQuery()), $query);
            $file_id = $query['fileId'] ?? '';
            if ($file_id === "82") {
              $file = file_get_contents(__DIR__ . "/../../../fixtures/assets/file_sample.jpg");
              $content_type = 'image/jpg';
            }
            if ($file_id === "83") {
              $file = file_get_contents(__DIR__ . "/../../../fixtures/assets/file_pdf_sample.pdf");
              $content_type = 'application/pdf';
            }
            if ($file_id === "84") {
              $file = file_get_contents(__DIR__ . "/../../../fixtures/assets/file_mp4_sample.mp4");
              $content_type = 'video/mp4';
            }

            return new FulfilledPromise(
              new Response(200, [
                'Content-Type' => $content_type,
                'Content-Length' => strlen($file),
              ], $file)
            );
          }
        }
        if (!str_contains($request->getUri()->getHost(), 'nextcloud.example.org')) {
          return $handler($request, $options);
        }

        $this->logger->warning(
          sprintf("The DAM client requested '%s' which is not mocked", $request->getUri())
        );
        throw new \RuntimeException('Request URI not mocked: ' . $request->getUri());
      };
    };
  }

}
