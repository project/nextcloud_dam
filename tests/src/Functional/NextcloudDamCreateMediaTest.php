<?php

namespace Drupal\Tests\nextcloud_dam\Functional;

use Drupal\Component\Serialization\Json;

/**
 * Tests media creation using Nextcloud DAM and filepicker widget.
 *
 * @group nextcloud_dam
 */
class NextcloudDamCreateMediaTest extends NextcloudDamTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->setUpContentTypesForMedia();
    // It won't need all the roles defined in parent dedicated function.
    $this->adminUser = $this->drupalCreateUser([
      'create media_entity_content content',
      'access content',
      'access nextcloud_filepicker entity browser pages',
      // Not needed for this test, just for debugging.
      'administer media',
      'administer media types',
    ]);
  }

  /**
   * Create media entities from Nextcloud picker results.
   */
  public function testCreateMediaEntities() {
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_results.json');
    $selection_array = JSON::decode($selection);
    $nextcloud_info = $selection_array['shareLinks'][0];

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];
    // Get configured media type for images(image type name is not suffixed as
    // document and video)
    $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.media_type");
    $this->assertEquals("nextcloud_image", $ebrowser_media_type);

    $image_type = \Drupal::entityTypeManager()->getStorage('media_type')->load($ebrowser_media_type);
    $image_source_field = $image_type->getSource()->getConfiguration()['source_field'];
    $storage_media = \Drupal::entityTypeManager()->getStorage('media');
    $image_media = $storage_media->create([
      'bundle' => $image_type->id(),
      $image_source_field => $nextcloud_info['data']['url'],
      // Fake tbd label, because name is mandatory in database level
      // will clear after to let source mapping replace it.
      'name' => '***TBD***',
    ]);
    // Clear label to let source mapping replace it.
    $image_media->set('name', '');

    $image_media->getSource()->fillMetadata($image_media, $nextcloud_info['data'], $ebrowser_config->get('relationship'));

    // Assert that we have no media created yet.
    $this->assertEquals(0, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));

    // Force Replace field values with what's mapped.
    // $image_media->prepareSave();
    $image_media->save();

    // Obtaining field_map config from source mapping and check here the
    // capturing.
    $image_type_fieldmap = $image_type->getFieldMap();
    /** @var \Drupal\media\MediaInterface $media */
    foreach ([$image_media] as $media) {
      switch ($media->get('field_nextcloud_id')->value) {
        case '82':
          // TEST metadata mapping.
          // filter duplicated retaining last mapping.
          $deduped_image_type_fieldmap = array_unique(array_reverse($image_type_fieldmap));
          foreach ($deduped_image_type_fieldmap as $metadata_attribute => $entity_field_name) {
            if ($media->{$entity_field_name}) {
              $actual_value = $media->{$entity_field_name}->value;
              $expected_value = $media->getSource()->getMetadata($media, $metadata_attribute);
              $this->assertEquals($expected_value, $actual_value, "Checking that value of metadata attribute $metadata_attribute gets copied to field $entity_field_name");
            }
          }
          $this->assertEquals('nextcloud_image', $media->bundle());
          $this->assertEquals('file_sample', $media->label());
          break;

        default:
          $this->fail('Unexpected media ' . $media->label());
          break;
      }
    }
  }

  /**
   * Create media entities from Nextcloud picker various filetypes results.
   */
  public function testCreateMediaEntitiesVariousTypes() {
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_multiple_types_results.json');
    $selection_array = JSON::decode($selection);
    $nextcloud_info = [];
    $nextcloud_info['media_type'] = $selection_array['shareLinks'][0];
    $nextcloud_info['media_type_document'] = $selection_array['shareLinks'][1];
    $nextcloud_info['media_type_video'] = $selection_array['shareLinks'][2];

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];

    // Assert that we have no media created yet.
    $this->assertEquals(0, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));

    // Assert that we have 3 media bundles created from config.
    $this->assertEquals(3, count(\Drupal::entityTypeManager()->getStorage('media_type')->loadMultiple()));

    $media_types_media_bundles_mapping = [
      "nextcloud_image" => "media_type",
      "nextcloud_document" => "media_type_document",
      "nextcloud_video" => "media_type_video",
    ];
    // Create each media of each type.
    foreach ($media_types_media_bundles_mapping as $media_bundle => $media_type_name) {
      // Save the media type to media bundle mapping.
      $ebrowser_media_type = $ebrowser_config->set("widgets.$ebrowser_widget_id.settings.$media_type_name", $media_bundle);
      $ebrowser_media_type->save();

      // Get configured media type for images(image type name is not suffixed
      // as document and video)
      $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.$media_type_name");

      // Mapping is done.
      $this->assertEquals($media_bundle, $ebrowser_media_type);

      $media_type = \Drupal::entityTypeManager()->getStorage("media_type")->load($ebrowser_media_type);
      $media_type_source_field = $media_type->getSource()->getConfiguration()['source_field'];
      $storage_media = \Drupal::entityTypeManager()->getStorage('media');
      $media = $storage_media->create([
        'bundle' => $media_type->id(),
        $media_type_source_field => $nextcloud_info[$media_type_name]['data']['url'],
        'name' => $nextcloud_info[$media_type_name]['data']['label'],
      ]);
      // Clear label to let source mapping replace it.
      $media->set('name', '');

      $media->getSource()->fillMetadata($media, $nextcloud_info[$media_type_name]['data'], $ebrowser_config->get('relationship'));
      // Force Replace field values with what's mapped.
      // $media->prepareSave();
      $media->save();
      // Obtaining field_map config from source mapping and check here the
      // capturing.
      $media_type_fieldmap = $media_type->getFieldMap();
      // TEST metadata mapping.
      // filter duplicated retaining last mapping.
      $deduped_media_type_fieldmap = array_unique(array_reverse($media_type_fieldmap));
      foreach ($deduped_media_type_fieldmap as $metadata_attribute => $entity_field_name) {
        if ($media->{$entity_field_name}) {
          $this->assertEquals($media->getSource()->getMetadata($media, $metadata_attribute), $media->{$entity_field_name}->value, "Checking that value of metadata attribute $metadata_attribute gets copied to field $entity_field_name");
        }
      }
      switch ($media->get('field_nextcloud_id')->value) {
        case '82':
          $this->assertEquals('nextcloud_image', $media->bundle());
          $this->assertEquals('file_sample', $media->label());
          break;

        case '83':
          $this->assertEquals('nextcloud_document', $media->bundle());
          $this->assertEquals('file_pdf_sample', $media->label());
          break;

        case '84':
          $this->assertEquals('nextcloud_video', $media->bundle());
          $this->assertEquals('file_mp4_sample', $media->label());
          break;

        default:
          $this->fail('Unexpected media ' . $media->label());
          break;
      }
    }
    // Assert that we have 3 media created yet.
    $this->assertEquals(3, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));
  }

  /**
   * Test create media entities on Nextcloud search widget.
   */
  public function testMultipleServerMediaEntities() {
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_multiple_servers_results.json');
    $selection_array = JSON::decode($selection);
    $nextcloud_info_server_a = $selection_array['shareLinks'][0];
    $nextcloud_info_server_b = $selection_array['shareLinks'][1];

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];
    // Get configured media type for images(image type name is not suffixed as
    // document and video)
    $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.media_type");
    $this->assertEquals("nextcloud_image", $ebrowser_media_type);

    $image_type = \Drupal::entityTypeManager()->getStorage('media_type')->load($ebrowser_media_type);
    $image_source_field = $image_type->getSource()->getConfiguration()['source_field'];
    $storage_media = \Drupal::entityTypeManager()->getStorage('media');

    $image_media_server_a = $storage_media->create([
      'bundle' => $image_type->id(),
      $image_source_field => $nextcloud_info_server_a['data']['url'],
      'name' => $nextcloud_info_server_a['data']['label'],
    ]);
    // Clear label to let source mapping replace it.
    $image_media_server_a->set('name', '');

    $image_media_server_a->getSource()->fillMetadata($image_media_server_a, $nextcloud_info_server_a['data'], $ebrowser_config->get('relationship'));

    $image_media_server_b = $storage_media->create([
      'bundle' => $image_type->id(),
      $image_source_field => $nextcloud_info_server_b['data']['url'],
      'name' => $nextcloud_info_server_b['data']['label'],
    ]);
    // Clear label to let source mapping replace it.
    $image_media_server_b->set('name', '');

    $image_media_server_b->getSource()->fillMetadata($image_media_server_b, $nextcloud_info_server_b['data'], $ebrowser_config->get('relationship'));

    // Assert that we have no media created yet.
    $this->assertEquals(0, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));
    $image_media_server_a->save();
    $image_media_server_b->save();
    $this->assertEquals(2, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));

    // TEST each media mapping type.
    /** @var \Drupal\media\MediaInterface $media */
    foreach ([$image_media_server_a, $image_media_server_b] as $media) {
      switch ($media->get('field_nextcloud_url')->value) {
        case 'https://nextcloud.otherexample.org/s/abcdefghiJKLMNo':
          // Server is correct despite id on both share id are the same.
          $this->assertEquals('https://nextcloud.otherexample.org', $media->getSource()->getMetadata($media, 'server'));
          $this->assertEquals('nextcloud_image', $media->bundle());
          $this->assertEquals('file_sample', $media->label());
          break;

        case 'https://nextcloud.example.org/s/abcdefghiJKLMNo':
          $this->assertEquals('https://nextcloud.example.org', $media->getSource()->getMetadata($media, 'server'));
          $this->assertEquals('nextcloud_image', $media->bundle());
          $this->assertEquals('file_sample', $media->label());
          break;

        default:
          $this->fail('Unexpected media ' . $media->label());
          break;
      }
    }
  }

  /**
   * Create media entities on Nextcloud file picker widget.
   */
  public function testCreateMediaEntitySingleEntityBrowserUi() {
    $assert_session = $this
      ->assertSession();
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_results.json');

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];
    // Get configured media type for images(image type name is not suffixed as
    // document and video)
    $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.media_type");
    $this->assertEquals("nextcloud_image", $ebrowser_media_type);

    // Test autocreate entities when using entitybrowser.
    $this->drupalGet('node/add/media_entity_content');
    $this->drupalGet('/entity-browser/modal/nextcloud_filepicker');
    $assert_session->hiddenFieldExists('nextcloud_selection')->setValue($selection);
    $this->getSession()->getPage()->pressButton('Select assets');
    $this->assertEquals(1, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));

    // Use the same assets and check if entities are not re-created.
    $this->drupalGet('/entity-browser/modal/nextcloud_filepicker');
    $assert_session->hiddenFieldExists('nextcloud_selection')->setValue($selection);
    $this->getSession()->getPage()->pressButton('Select assets');
    $medias = \Drupal::entityTypeManager()->getStorage('media')->loadMultiple();
    $this->assertEquals(1, count($medias));
  }

  /**
   * Create media entities on Nextcloud file picker widget.
   */
  public function testCreateMultipleMediaWithEntityBrowserUi() {
    $assert_session = $this
      ->assertSession();
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_multiple_types_results.json');

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];
    // Get configured media type for images(image type name is not suffixed as
    // document and video)
    $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.media_type");
    $this->assertEquals("nextcloud_image", $ebrowser_media_type);

    // Test autocreate entities when using entitybrowser.
    $this->drupalGet('node/add/media_entity_content');
    $this->drupalGet('/entity-browser/modal/nextcloud_filepicker');
    $assert_session->hiddenFieldExists('nextcloud_selection')->setValue($selection);
    $this->getSession()->getPage()->pressButton('Select assets');
    $medias = \Drupal::entityTypeManager()->getStorage('media')->loadMultiple();
    $this->assertEquals(3, count($medias));
  }

  /**
   * Create media entities from Nextcloud picker various filetypes results.
   */
  public function testCreateMediaEntitiesVariousTypesNoUi() {
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_multiple_types_results.json');
    $selection_array = JSON::decode($selection);
    $nextcloud_info = [];
    $nextcloud_info['media_type'] = $selection_array['shareLinks'][0];
    $nextcloud_info['media_type_document'] = $selection_array['shareLinks'][1];
    $nextcloud_info['media_type_video'] = $selection_array['shareLinks'][2];

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    // Assert that there is a defined widget for entitybrowser.
    $this->assertEquals(1, count($ebrowser_config->get('widgets')));
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];

    // Assert that we have no media created yet.
    $this->assertEquals(0, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));

    // Assert that we have 3 media bundles created from config.
    $this->assertEquals(3, count(\Drupal::entityTypeManager()->getStorage('media_type')->loadMultiple()));

    $media_types_media_bundles_mapping = [
      "nextcloud_image" => "media_type",
      "nextcloud_document" => "media_type_document",
      "nextcloud_video" => "media_type_video",
    ];

    $medias_created = [];
    // Create each media of each type.
    foreach ($media_types_media_bundles_mapping as $media_bundle => $media_type_name) {
      // Save the media type to media bundle mapping.
      $ebrowser_media_type = $ebrowser_config->set("widgets.$ebrowser_widget_id.settings.$media_type_name", $media_bundle);
      $ebrowser_media_type->save();

      // Get configured media type for images(image type name is not suffixed as
      // document and video)
      $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.$media_type_name");

      // Mapping is done.
      $this->assertEquals($media_bundle, $ebrowser_media_type);

      $media_type = \Drupal::entityTypeManager()->getStorage("media_type")->load($ebrowser_media_type);
      $media_type_source_field = $media_type->getSource()->getConfiguration()['source_field'];
      $storage_media = \Drupal::entityTypeManager()->getStorage('media');
      $media = $storage_media->create([
        'bundle' => $media_type->id(),
        $media_type_source_field => $nextcloud_info[$media_type_name]['data']['url'],
        'name' => $nextcloud_info[$media_type_name]['data']['label'],
      ]);
      // Clear label to let source mapping replace it.
      $media->set('name', '');

      $media->getSource()->fillMetadata($media, $nextcloud_info[$media_type_name]['data'], $ebrowser_config->get('relationship'));
      $media->save();
      // Obtaining field_map config from source mapping and check here the
      // capturing.
      $media_type_fieldmap = $media_type->getFieldMap();
      // TEST metadata mapping.
      // filter duplicated retaining last mapping.
      $deduped_media_type_fieldmap = array_unique(array_reverse($media_type_fieldmap));
      foreach ($deduped_media_type_fieldmap as $metadata_attribute => $entity_field_name) {
        if ($media->{$entity_field_name}) {
          $this->assertEquals($media->getSource()->getMetadata($media, $metadata_attribute), $media->{$entity_field_name}->value, "Checking that value of metadata attribute $metadata_attribute gets copied to field $entity_field_name");
        }
      }
      switch ($media->get('field_nextcloud_id')->value) {
        case '82':
          $this->assertEquals('nextcloud_image', $media->bundle());
          $this->assertEquals('file_sample', $media->label());
          break;

        case '83':
          $this->assertEquals('nextcloud_document', $media->bundle());
          $this->assertEquals('file_pdf_sample', $media->label());
          break;

        case '84':
          $this->assertEquals('nextcloud_video', $media->bundle());
          $this->assertEquals('file_mp4_sample', $media->label());
          break;

        default:
          $this->fail('Unexpected media ' . $media->label());
          break;
      }
      $medias_created[] = $media->id();
    }
    // Assert that we have 3 media created yet.
    $this->assertEquals(3, count(\Drupal::entityTypeManager()->getStorage('media')->loadMultiple()));
  }

}
