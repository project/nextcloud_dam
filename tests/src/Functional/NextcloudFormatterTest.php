<?php

namespace Drupal\Tests\nextcloud_dam\Functional;

/**
 * Tests nextcloud field formatter.
 *
 * @group nextcloud_dam
 */
class NextcloudFormatterTest extends NextcloudDamTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    $this->setUpContentTypesForMedia();
    $this->setUpUsersRoles();
  }

  /**
   * Create Node with its media entities and test formatter.
   */
  public function testMediaFormatter() {
    $medias_created = $this->addMediaEntitiesVariousTypesNoUi();
    $node1 = $this
      ->drupalCreateNode([
        'type' => 'media_entity_content',
        'field_reference' => $medias_created,
      ]);
    $this->drupalGet("node/{$node1->id()}");
    $this->assertSession()->responseContains('https://nextcloud.example.org/s/abcdefghiJKLMNo/preview');
    $this->assertSession()->responseContains('https://nextcloud.example.org/s/bbcdefghiJKLMNo/preview');
    $this->assertSession()->responseContains('https://nextcloud.example.org/s/cbcdefghiJKLMNo/preview');
  }

}
