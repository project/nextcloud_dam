<?php

namespace Drupal\Tests\nextcloud_dam\Functional;

/**
 * Tests nextcloud usage route and its media update links.
 *
 * @group nextcloud_dam
 */
class NextcloudUsageTest extends NextcloudDamTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {

    parent::setUp();
    $this->setUpNextcloudInstances();
    $this->setUpContentTypesForMedia();
    $this->setUpUsersRoles();

  }

  /**
   * Update the metadata using an immediate batch queue from usage page.
   */
  public function testBatchUpdateMetadata() {
    $medias_created = $this->addMediaEntitiesVariousTypesNoUi();
    // Create the node to reference media.
    $node1 = $this
      ->drupalCreateNode([
        'type' => 'media_entity_content',
        'field_reference' => $medias_created,
      ]);
    $this->drupalGet("node/{$node1->id()}");
    $this->drupalGet("node/{$node1->id()}/nextcloud-usage");

    // Access is denied as not having permission to refresh.
    $this->assertSession()->responseContains('Access denied');

    $this->testRole->grantPermission('refresh media nextcloud metadata');
    $this->testRole->save();
    $this->drupalGet("node/{$node1->id()}/nextcloud-usage");
    $this->assertSession()->responseContains('file sample');
    $this->assertSession()->responseContains('file pdf sample');
    $this->assertSession()->responseContains('file mp4 sample');

    $this->submitForm([
      'media_using_nextcloud[1]' => TRUE,
      'media_using_nextcloud[2]' => TRUE,
      'media_using_nextcloud[3]' => TRUE,
    ], 'Apply to selected items');

    // Socialauth not defined so client could not connect.
    $this->assertSession()->responseContains('Nextcloud DAM not defined, could not connect.');

    // Add simulated social auth to perform the client connection.
    $this->addSocialAuth();
    $this->drupalGet("node/{$node1->id()}/nextcloud-usage");
    $this->submitForm([
      'media_using_nextcloud[1]' => TRUE,
      'media_using_nextcloud[2]' => TRUE,
      'media_using_nextcloud[3]' => TRUE,
    ], 'Apply to selected items');

    $this->assertSession()->responseContains('was applied to 3 items.');
    $this->assertSession()->responseContains('file sample updated');
    $this->assertSession()->responseContains('file pdf sample updated');
    $this->assertSession()->responseContains('file mp4 sample updated');
  }

  /**
   * Update metadata of a single media using the route defined for this.
   */
  public function testUpdateSingleMetadata() {
    $medias_created = $this->addMediaEntitiesVariousTypesNoUi();
    // Add node to contain media.
    $node1 = $this
      ->drupalCreateNode([
        'type' => 'media_entity_content',
        'field_reference' => $medias_created,
      ]);
    $single_media_id = array_shift($medias_created);
    $this->drupalGet("node/{$node1->id()}/nextcloud-usage");

    // Access is denied as not having permission to refresh.
    $this->assertSession()->responseContains('Access denied');

    // Needs testRole to be set.
    $this->testRole->grantPermission('refresh media nextcloud metadata');
    $this->testRole->save();
    $this->drupalGet("node/{$node1->id()}/nextcloud-usage");
    $this->assertSession()->responseContains('file sample');

    $this->drupalGet("/media/{$single_media_id}/update_nextcloud_metadata", ["query" => ["destination" => "node/{$node1->id()}/nextcloud-usage"]]);
    // Socialauth not defined so client could not connect.
    $this->assertSession()->responseContains('Nextcloud DAM not defined, could not connect.');

    // Add simulated social auth to perform the client connection.
    $this->addSocialAuth();

    // @todo Perform update going to route to update metadata.
    $this->drupalGet("/media/{$single_media_id}/update_nextcloud_metadata", ["query" => ["destination" => "node/{$node1->id()}/nextcloud-usage"]]);

    // Assert That file label has been updated.
    $this->assertSession()->responseContains('file sample updated');
  }

}
