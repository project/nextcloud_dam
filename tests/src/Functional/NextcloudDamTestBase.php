<?php

namespace Drupal\Tests\nextcloud_dam\Functional;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\TestFileCreationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\user\Entity\Role;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Defines a base class for Nextcloud dam.
 *
 * @group nextcloud_dam
 */
abstract class NextcloudDamTestBase extends BrowserTestBase {

  use TestFileCreationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'nextcloud_dam',
    'node',
    'file',
    'social_auth_nextcloud',
    'nextcloud_dam_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Role for testing.
   *
   * @var \Drupal\user\RoleInterface
   */
  protected $testRole;

  /**
   * Mock settings.php to add nextcloud instances. Rewrites the settings.php.
   */
  protected function setUpNextcloudInstances(): void {
    $this
      ->writeSettings([
        'settings' => [
          'social_auth_nextcloud_instances' => (object) [
            'value' => ["nextcloud.example.org"],
            'required' => TRUE,
          ],
        ],
      ]);
    $this->rebuildContainer();
  }

  /**
   * Adds users and Roles.
   */
  protected function setUpUsersRoles(): void {

    $this->testRole = Role::create(['id' => 'editor', 'label' => 'editor']);
    $this->testRole->grantPermission('create media_entity_content content');
    $this->testRole->grantPermission('access content');
    $this->testRole->grantPermission('access nextcloud_filepicker entity browser pages');
    $this->testRole->grantPermission('administer social api authentication');
    $this->testRole->save();

    $this->adminUser = $this->drupalCreateUser();
    $this->adminUser->addRole($this->testRole->id());

    $this->adminUser->save();
  }

  /**
   * Create content types with its form and view display components.
   */
  protected function setUpContentTypesForMedia(): void {
    $this->drupalCreateContentType([
      'type' => 'media_entity_content',
      'name' => 'Media Type',
    ]);

    FieldStorageConfig::create([
      'field_name' => 'field_reference',
      'type' => 'entity_reference',
      'entity_type' => 'node',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings' => [
        'target_type' => 'media',
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => 'field_reference',
      'entity_type' => 'node',
      'bundle' => 'media_entity_content',
      'label' => 'Reference',
      'settings' => [
        'handler' => 'default:media',
        'handler_settings' => [
          'target_bundles' => [
            'nextcloud_image' => 'nextcloud_image',
          ],
        ],
      ],
    ])->save();
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $form_display */
    $form_display = $this->container->get('entity_type.manager')
      ->getStorage('entity_form_display')
      ->load('node.media_entity_content.default');

    $form_display->setComponent('field_reference', [
      'type' => 'entity_browser_entity_reference',
      'settings' => [
        'entity_browser' => 'nextcloud_filepicker',
        'open' => TRUE,
      ],
    ])->save();

    /** @var \Drupal\Core\Entity\Display\EntityDisplayInterface $view_display */
    $view_display = $this->container->get('entity_type.manager')
      ->getStorage('entity_view_display')
      ->load('node.media_entity_content.default');

    $view_display->setComponent('field_reference', [
      "type" => 'media_thumbnail',
      "settings" => [
        'image_link' => '',
        'image_style' => '',
        'image_loading' => [
          'attribute' => 'eager',
        ],
      ],
    ]);
    $view_display->save();
  }

  /**
   * Create media entities from Nextcloud picker various filetypes results.
   *
   * @return array
   *   Returns array of medias created.
   */
  protected function addMediaEntitiesVariousTypesNoUi() {
    $this
      ->drupalLogin($this->adminUser);

    $selection = file_get_contents(__DIR__ . '/../../fixtures/nextcloud_picker_multiple_types_results.json');
    $selection_array = JSON::decode($selection);
    $nextcloud_info = [];
    $nextcloud_info['media_type'] = $selection_array['shareLinks'][0];
    $nextcloud_info['media_type_document'] = $selection_array['shareLinks'][1];
    $nextcloud_info['media_type_video'] = $selection_array['shareLinks'][2];

    /** @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig **/
    $ebrowser_config = $this->config('entity_browser.browser.nextcloud_filepicker');
    ;
    $ebrowser_widget_id = array_keys($ebrowser_config->get('widgets'))[0];

    $media_types_media_bundles_mapping = [
      "nextcloud_image" => "media_type",
      "nextcloud_document" => "media_type_document",
      "nextcloud_video" => "media_type_video",
    ];
    $medias_created = [];
    // Create each media of each type.
    foreach ($media_types_media_bundles_mapping as $media_bundle => $media_type_name) {
      // Save the media type to media bundle mapping.
      $ebrowser_media_type = $ebrowser_config->set("widgets.$ebrowser_widget_id.settings.$media_type_name", $media_bundle);
      $ebrowser_media_type->save();

      // Get configured media type for images(image type name is not suffixed as
      // document and video)
      $ebrowser_media_type = $ebrowser_config->get("widgets.$ebrowser_widget_id.settings.$media_type_name");

      $media_type = \Drupal::entityTypeManager()->getStorage("media_type")->load($ebrowser_media_type);
      $media_type_source_field = $media_type->getSource()->getConfiguration()['source_field'];

      $storage_media = \Drupal::entityTypeManager()->getStorage('media');
      $media = $storage_media->create([
        'bundle' => $media_type->id(),
        $media_type_source_field => $nextcloud_info[$media_type_name]['data']['url'],
        // Will be cleared later.
        'name' => '****TBD****',
      ]);
      // Clear label to let source mapping replace it.
      $media->set('name', '');

      $media->getSource()->fillMetadata($media, $nextcloud_info[$media_type_name]['data'], $ebrowser_config->get('relationship'));
      $media->save();
      $medias_created[] = $media->id();
    }

    return $medias_created;
  }

  /**
   * Creates the social auth for the specified user.
   *
   * @param array $values
   *   The values used to create the social auth.
   */
  protected function addSocialAuth(array $values = []): void {
    // Use next approach to perform session writes.
    // https://www.drupal.org/docs/automated-testing/phpunit-in-drupal/working-with-user-sessions-in-tests
    $sid = $this->getSession()->getCookie($this->getSessionName());
    $session_data = $this->container->get('session_handler.storage')->read($sid);
    $session_data = $this->unserializePhp($session_data);

    $dataHandler = \Drupal::getContainer()->get('social_auth.data_handler');
    $userAuthenticator = \Drupal::getContainer()->get('social_auth.user_authenticator');
    // Associates a provider.
    $userAuthenticator->setPluginId('social_auth_nextcloud:nextcloud.example.org');
    $dataHandler->setSessionPrefix('social_auth_nextcloud:nextcloud.example.org');

    if (empty($values)) {
      $values = [
        'user_id' => $this->adminUser->id(),
        'plugin_id' => 'social_auth_nextcloud:nextcloud.example.org',
        'provider_user_id' => 'nc_user',
        'additional_data' => ['foo' => 'bar'],
        'token' => 'token_test',
      ];
    }
    $social_auth_storage = \Drupal::entityTypeManager()->getStorage('social_auth');
    $social_auth = $social_auth_storage->create($values);
    $social_auth->save();

    $options = ['access_token' => 'access_token', 'expires_in' => 100];
    $acctoken = new AccessToken($options);
    // We couldnt do it directly with: so doing it with un serialized
    // session_data from db
    // $dataHandler->set('access_token', $acctoken); // or
    // $userAuthenticator->associateNewProvider(
    // 'social_auth_nextcloud:nextcloud.example.org',
    // $acctoken);.
    $session_data['_sf2_attributes']['social_auth_nextcloud:nextcloud.example.org_access_token'] = $acctoken;

    $this->writeSession($this->adminUser->id(), $sid, $this->serializePhp($session_data));
    $this->serializePhp($session_data);
  }

  /**
   * A way to configure nextcloud provider using social auth UI form.
   */
  protected function setUpNextcloudProviderViaUi() {
    // Configure with UI the nextcloud provider.
    $this->drupalGet("/admin/config/social-api/social-auth/nextcloud/nextcloud:nextcloud.example.org");
    $edit = [
      'client_id' => $this->randomString(10),
      'client_secret' => $this->randomString(10),
      'nextcloud_url' => 'https://nextcloud.example.org',
    ];
    // Save social auth nextcloud configuration.
    $this->submitForm($edit, 'Save configuration');

    // Simulate obtaining the oauth token, the auth token is hardcoded to
    // authtoken123 in middleware, the state token is what we need to obtain.
    $this->drupalGet("/user/login/nextcloud:nextcloud.example.org");
    $oauth_server_response = JSON::decode($this->getTextContent());
    $this->drupalGet("/user/login/nextcloud:nextcloud.example.org/callback", [
      "query" => [
        "code" => $oauth_server_response['code'],
        "state" => $oauth_server_response['state'],
      ],
    ]);
  }

  /**
   * Converts session data from the database into an array of data.
   *
   * This is necessary because PHP's methods write to the $_SESSION superglobal,
   * and we want to work with session data that isn't ours.
   *
   * See https://stackoverflow.com/questions/530761/how-can-i-unserialize-session-data-to-an-arbitrary-variable-in-php
   *
   * @param string $session_data
   *   The serialised session data. (Note this is not the same serialisation
   *   format as used by serialize().)
   *
   * @return array
   *   An array of data.
   */
  protected function unserializePhp($session_data) {
    $return_data = [];
    $offset = 0;
    while ($offset < strlen($session_data)) {
      if (!strstr(substr($session_data, $offset), "|")) {
        throw new \Exception("invalid data, remaining: " . substr($session_data, $offset));
      }
      $pos = strpos($session_data, "|", $offset);
      $num = $pos - $offset;
      $varname = substr($session_data, $offset, $num);
      $offset += $num + 1;
      $data_serialized = substr($session_data, $offset);
      $data = @unserialize(trim($data_serialized), ['allowed_classes' => FALSE]);
      $return_data[$varname] = $data;
      $offset += strlen(serialize($data));
    }
    return $return_data;
  }

  /**
   * Converts an array of data into a session data string.
   *
   * This is necessary because PHP's methods write to the $_SESSION superglobal,
   * and we want to work with session data that isn't ours.
   *
   * See https://stackoverflow.com/questions/530761/how-can-i-unserialize-session-data-to-an-arbitrary-variable-in-php
   *
   * @param array $data
   *   The session data.
   *
   * @return string
   *   The serialised data. (Note this is not the same serialisation format as
   *   used by serialize().
   */
  protected function serializePhp($data) {
    $return_data = '';
    foreach ($data as $key => $key_data) {
      $return_data .= "{$key}|" . serialize($key_data);
    }

    return $return_data;
  }

  /**
   * Write data to the given session.
   *
   * This exists because we can't use
   * \Drupal\Core\Session\SessionHandler::write() as that assumes the current
   * session is being written, and will fail within tests as no session exists.
   *
   * @param int $uid
   *   The user ID.
   * @param int $sid
   *   The session ID.
   * @param string $value
   *   The session data. Use serializePhp() to format this.
   */
  protected function writeSession($uid, $sid, $value) {
    $fields = [
      'uid' => $uid,
      'hostname' => 'testing',
      'session' => $value,
      'timestamp' => \Drupal::time()->getRequestTime(),
    ];
    $this->container->get('database')->merge('sessions')
      ->keys(['sid' => Crypt::hashBase64($sid)])
      ->fields($fields)
      ->execute();
  }

}
